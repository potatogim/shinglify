package shinglify

type UIBackend interface {
	Compatible() bool
	Init(inst *Inst) error
	ScreenUpdate(wms WMState, events []Evt)
}
