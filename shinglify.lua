-- enable the XFCE GenMon integration. See the README for details.
if (evt_type()=="Init") then

    enableui_xfcegenmon(true,"bgcolor='#a0d0b0' fgcolor='#000000' ","fgcolor='#c0c0c0'","default",false)

    screen_layout=get_screen_layout()

    print(screen_layout)

    local found=false


    if((screen_layout=="DP1-1.VX3276-QHD.V9W201241091.2560.0.2560.1440;DP1-2.VX3276-QHD.V9W201241100.0.0.2560.1440;HDMI1.DELL U2415.7MT0187H1JVU.5120.240.1920.1200")or(
        screen_layout=="DP1-1.VX3276-QHD.V9W201241091.2560.0.2560.1440;DP1-2.VX3276-QHD.V9W201241100.0.0.2560.1440;HDMI1.DELL U2415.7MT0187H1JVU.5120.19.1920.1200"))then
        print("Layout 1")
        move_xfcepanel_to_screen(1,"DP1-1")
        move_xfcepanel_to_screen(5,"DP1-2")
        move_xfcepanel_to_screen(6,"HDMI1")

        add_screen_tag("DP1-1","1","2","3")
        add_screen_tag("DP1-2","4","5")
        add_screen_tag("HDMI1","6","7")
        found=true
         
    end

    if(screen_layout=="DP1-1.LEN L27q-30.U162V0BA.2560.0.2560.1440;DP1-2.LEN L27q-30.U162V0BF.0.0.2560.1440")then
        print("nutheUnten")
        move_xfcepanel_to_screen("6","DP1-1")
        move_xfcepanel_to_screen("1","DP1-2")
        move_xfcepanel_to_screen("5","")

        add_screen_tag("DP1-2","1","2","3")
        add_screen_tag("DP1-1","4","5")
        found=true
    end

    if((screen_layout=="DP1-1.DELL P2314H.J8J3144NBC1S.3840.0.1920.1080;DP1-2.DELL P2314H.J8J3144DEFTS.1920.0.1920.1080;eDP1.2401.0.0.393.1920.1200")or(
        screen_layout=="DP1-1.PL2792H.1153701601268.3840.0.1920.1080;DP1-2.PL2792H.1153701601261.1920.0.1920.1080;eDP1.2401.0.0.0.1920.1200"))then
        print("Layout 2")
        move_xfcepanel_to_screen("5","DP1-1")
        move_xfcepanel_to_screen("1","DP1-2")
        move_xfcepanel_to_screen("6","")

        add_screen_tag("DP1-2","1","2","3")
        add_screen_tag("DP1-1","4","5")
--        add_screen_tag("eDP1","6","7")
        found=true
    end

    if(not found)then
        add_screen_tag("eDP1","1","2","3")
        move_xfcepanel_to_screen("1","eDP1")
        move_xfcepanel_to_screen("5","")
        move_xfcepanel_to_screen("6","")
    end

    print("found:",found)

end

-- Set the default tiler. This does not actually do much, but these are the options for tilers.

--set_tiler(tiler_grid)
--set_tiler(tiler_masterstack)
--set_tiler(tiler_vtabbed)
--set_tiler(tiler_vtabbed_stagger)


-- Set the gaps globally. Inner, outer, single
-- Inner is the gap between windows when tiled
-- Outer is the gap between windows and the border of the usable screen space (considering panels)
-- Single is the gap around a single window on a tiled screen 
set_gaps("",5,5,5)

-- Set up navigation keys
-- navigate spatially among visible (non-hidden, not obstructed) windows
key_bind("Mod4-shift-left",navigate_left)
key_bind("Mod4-shift-right",navigate_right)
key_bind("Mod4-shift-up",navigate_up)
key_bind("Mod4-shift-down",navigate_down)
   
-- navigate to next/prev window globally
key_bind("Mod4-prior",navigate_prev)
key_bind("Mod4-next",navigate_next)

-- navigate between screens
key_bind("Mod4-shift-prior",navigate_prev_on_screen)
key_bind("Mod4-shift-next",navigate_next_on_screen)

-- navigate between tags on the same screen
key_bind("Mod4-control-prior",navigate_prev_tag_on_screen)
key_bind("Mod4-control-next",navigate_next_tag_on_screen)

-- move active window to next screen
key_bind("Mod4-control-left",move_window_left)
key_bind("Mod4-control-right",move_window_right)
key_bind("Mod4-control-up",move_window_up)
key_bind("Mod4-control-down",move_window_down)

-- Set a key combo to toggle maximized/normal window size

key_bind("Mod4-space",function() maximize_window(get_active_windowid(),2) end)

-- Set key combos for tags. Tags can be any text and there can be any number.


key_bind("Mod4-shift-1",function() set_active_window_tag("1") end)
key_bind("Mod4-shift-2",function() set_active_window_tag("2") end)
key_bind("Mod4-shift-3",function() set_active_window_tag("3") end)
key_bind("Mod4-shift-4",function() set_active_window_tag("4") end)
key_bind("Mod4-shift-5",function() set_active_window_tag("5") end)
key_bind("Mod4-shift-6",function() set_active_window_tag("6") end)
key_bind("Mod4-shift-7",function() set_active_window_tag("7") end)
key_bind("Mod4-shift-8",function() set_active_window_tag("8") end)
key_bind("Mod4-shift-9",function() set_active_window_tag("9") end)
key_bind("Mod4-shift-0",function() set_active_window_tag("") end)
key_bind("Mod4-shift-q",function() set_active_window_tag("1") end)
key_bind("Mod4-shift-w",function() set_active_window_tag("2") end)
key_bind("Mod4-shift-e",function() set_active_window_tag("3") end)
key_bind("Mod4-shift-r",function() set_active_window_tag("4") end)
key_bind("Mod4-shift-t",function() set_active_window_tag("5") end)
key_bind("Mod4-shift-y",function() set_active_window_tag("6") end)
key_bind("Mod4-shift-u",function() set_active_window_tag("7") end)
key_bind("Mod4-shift-i",function() set_active_window_tag("8") end)
key_bind("Mod4-shift-o",function() set_active_window_tag("9") end)
key_bind("Mod4-shift-p",function() set_active_window_tag("") end)
key_bind("Mod4-1",function() show_tag("1") end)
key_bind("Mod4-2",function() show_tag("2") end)
key_bind("Mod4-3",function() show_tag("3") end)
key_bind("Mod4-4",function() show_tag("4") end)
key_bind("Mod4-5",function() show_tag("5") end)
key_bind("Mod4-6",function() show_tag("6") end)
key_bind("Mod4-7",function() show_tag("7") end)
key_bind("Mod4-8",function() show_tag("8") end)
key_bind("Mod4-9",function() show_tag("9") end)

-- Set key combos for tiling

key_bind("Mod4-shift-a",function()set_tiler(get_active_screenid(),tiler_grid,false)end)
key_bind("Mod4-shift-s",function()set_tiler(get_active_screenid(),tiler_masterstack,false)end)
key_bind("Mod4-shift-d",function()set_tiler(get_active_screenid(),tiler_vtabbed,false)end)
key_bind("Mod4-shift-f",function()set_tiler(get_active_screenid(),nil,false)end)

