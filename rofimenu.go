package shinglify

import (
	"fmt"
	"os/exec"
	"strconv"
	"strings"
)

func (inst *Inst) RofiWindowSwitcher() int {
	wms := inst.WMBE.State(false)
	lines := []string{}
	wndIDs := []WindowID{}
	activeLine := -1
	for _, scr := range wms.Screens {
		lines = append(lines, "\t"+scr.Name)
		wndIDs = append(wndIDs, "")
		for _, wnd := range wms.Windows {
			if !wnd.Special && !wnd.Panel && !wnd.IsDesktop && wnd.PrimaryScreen == scr.ID {
				if wnd.Active {
					activeLine = len(lines)
					lines = append(lines, "* "+wnd.Title)
					wndIDs = append(wndIDs, wnd.ID)
				} else {
					lines = append(lines, ""+wnd.Title)
					wndIDs = append(wndIDs, wnd.ID)
				}
			}
		}
		_ = scr
	}
	_ = activeLine

	cmd := exec.Command("rofi", "-dmenu", "-format", "i:s", "-selected-row", fmt.Sprintf("%v", activeLine))
	cmdi, err := cmd.StdinPipe()
	if err != nil {
		inst.Log("Rofi error: %v", err)
		return 0
	}
	rofiIn := strings.Join(lines, "\n") + "\n"
	go func() {
		cmdi.Write([]byte(rofiIn))
		cmdi.Close()
	}()
	cmdo, err := cmd.Output()
	if err != nil {
		inst.Log("Rofi error: %v", err)
		return 0
	}
	stro := string(cmdo)
	colidx := strings.Index(stro, ":")
	lidx, err := strconv.Atoi(stro[:colidx])
	if err != nil {
		return 0
	}
	if lidx < 0 || lidx >= len(wndIDs) {
		return 0
	}
	wndID := wndIDs[lidx]
	if wndID == "" {
		return 0
	}
	wms = inst.WMBE.State(false)
	wnd := wms.GetWindow(wndID)
	if wnd == nil {
		return 0
	}
	if wnd.Tag != "" {
		inst.WMBE.SetScreenTag(wnd.PrimaryScreen, wnd.Tag)
	}

	inst.WMBE.Activate(wndID)

	return 0
}
