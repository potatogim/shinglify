package shinglify

import "image"

type WindowID = string
type ScreenID = string
type KeyBindDef = string

type WMBackend interface {
	Compatible() bool
	Init(inst *Inst) error
	Shut()
	RegisterKeyBind(keys KeyBindDef, evtDataPress interface{}, evtDataRelease interface{}) error
	EvtChan() chan Evt

	State(refresh bool) WMState
	Icon(wid WindowID) image.Image

	BeginChange() error
	ArrangeWindows([]WindowID) error
	Show(wid WindowID, show bool) error
	Move(wid WindowID, r Rct) error
	Maximize(wid WindowID, dir int) error // 0:unmaximize, 1=maximize, 2=toggle
	Activate(wid WindowID) error
	SetWindowTag(wid WindowID, tag Tag) error
	SetScreenTag(sid ScreenID, tag Tag) error
	GetScreenTag(sid ScreenID) (string, error)
	AddScreenTag(sid ScreenID, tag Tag) error
	RemScreenTag(sid ScreenID, tag Tag) error
	EndChange() error
}
