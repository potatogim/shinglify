package shinglify

import (
	"fmt"
	"image/png"
	"os"
	"os/exec"
	"sort"
	"strconv"
	"strings"
	"time"
	"unicode"

	"git.pixeltamer.net/gopub/logfunk"
	"git.pixeltamer.net/gopub/timestamp"
)

type XFCEGenMonInstance struct {
	Screen         string
	Filename       string
	PluginID       int
	PanelID        string
	PanelSize      int
	PanelPluginCnt int
	LastText       string
}

type XFCEGenMonUI struct {
	GenMons            []*XFCEGenMonInstance
	GenmonConfigDone   bool
	inst               *Inst
	log                logfunk.F
	runID              string
	iconDone           map[string]struct{}
	styleActive        string
	styleInactive      string
	styleInactiveFront string
	showIcon           bool
	showWindows        bool
}

func (ui *XFCEGenMonUI) Compatible() bool {
	return true
}

func (ui *XFCEGenMonUI) DetectGenmonConfig(wms WMState) {

	ui.GenMons = nil

	cmdo, err := exec.Command("xfconf-query", "-c", "xfce4-panel", "-p", "/", "-lv").Output()
	if err != nil {
		ui.log(logfunk.Error, "%v", err)
		return
	}

	panel2scr := make(map[string]string)
	panel2ppcnt := make(map[string]int)
	panel2size := make(map[string]int)
	genmonIDs := []int{}

	ui.log("Using screens: %v", func() string {
		sids := []string{}
		for _, scr := range wms.Screens {
			sids = append(sids, fmt.Sprintf("%v@%v", scr.ID, scr.Geometry))
		}
		return strings.Join(sids, ", ")
	}())

	/*
		for _, scr := range wms.Screens {
			fmt.Printf("Screen %v %v\n", scr.ID, scr.Geometry)
		}*/
	lines := strings.Split(string(cmdo), "\n")
	for _, line := range lines {
		parts := strings.Fields(line)
		if len(parts) != 2 {
			continue
		}
		//		fmt.Printf("P: %#v\n", parts)
		if strings.HasPrefix(parts[0], "/panels/") {
			pparams := strings.Split(parts[0], "/")
			//			fmt.Printf("PP: %#v  %v\n", pparams, parts[1])
			if len(pparams) == 4 {
				if pparams[3] == "size" {
					panelName := pparams[2]
					if sz, err := strconv.Atoi(parts[1]); err == nil {
						panel2size[panelName] = sz
					} else {
						panel2size[panelName] = 16
					}
				}
				if pparams[3] == "position" {
					panelName := pparams[2]
					posparts := strings.Split(parts[1], ";")
					x := 0
					y := 0
					for _, posp := range posparts {
						switch {
						case strings.HasPrefix(posp, "x="):
							x, _ = strconv.Atoi(posp[2:])
						case strings.HasPrefix(posp, "y="):
							y, _ = strconv.Atoi(posp[2:])
						}
					}
					//					fmt.Printf("PanelChk '%v' %v,%v\n", panelName, x, y)
					found := false
					for _, scr := range wms.Screens {
						if scr.Geometry.Hit(Pnt{X: x, Y: y}) {
							//							fmt.Printf("Panel '%v' %v,%v %v\n", panelName, x, y, scr.ID)
							panel2scr[panelName] = scr.ID
							found = true
							ui.log("Panel %v @ %v,%v: on screen %v", panelName, x, y, scr.ID)
						}
					}
					if !found {
						ui.log("Panel %v @ %v,%v: no matching screen found", panelName, x, y)
					}
				}
			}
		}
		if strings.HasPrefix(parts[0], "/plugins/") {
			if len(parts) == 2 {
				if parts[1] == "genmon" {
					ppath := strings.Split(parts[0], "/")
					if len(ppath) == 3 {
						if ppath[1] == "plugins" {
							idx := strings.Index(ppath[2], "-")
							if idx != -1 {
								id, err := strconv.Atoi(ppath[2][idx+1:])
								if err == nil {
									genmonIDs = append(genmonIDs, id)
								}
							}
						}
					}
				}
			}
		}
	}

	for panel := range panel2scr {
		cmdo, err = exec.Command("xfconf-query", "-c", "xfce4-panel", "-p", "/panels/"+panel+"/plugin-ids").Output()
		if err != nil {
			continue
		}
		lines := strings.Split(string(cmdo), "\n")
		numPlugins := 0
		for _, line := range lines {
			pluginID, err := strconv.Atoi(line)
			if err == nil {
				numPlugins++
				for _, gid := range genmonIDs {
					if gid == pluginID {
						cfg, err := os.ReadFile(os.ExpandEnv(fmt.Sprintf("$HOME/.config/xfce4/panel/genmon-%v.rc", gid)))
						if err != nil {
							fmt.Printf("RdErr %v\n", err)
							continue
						}
						rclines := strings.Split(string(cfg), "\n")
						match := false
						screen := panel2scr[panel]
						filename := ""
						for _, rcline := range rclines {
							parts := strings.Split(rcline, "=")
							if len(parts) != 2 {
								continue
							}
							if parts[0] == "Text" {
								if strings.HasPrefix(parts[1], "shinglify") {
									disp := parts[1][9:]
									if len(disp) > 1 {
										screen = disp[1:]
									}
									match = true
								}
							}
							if parts[0] == "Command" {
								if strings.HasPrefix(parts[1], "cat ") {
									filename = parts[1][4:]
								}
							}
						}
						if match && filename != "" {
							ui.GenMons = append(ui.GenMons, &XFCEGenMonInstance{
								Screen:         screen,
								Filename:       filename,
								PluginID:       gid,
								PanelID:        panel,
								PanelSize:      panel2size[panel],
								PanelPluginCnt: panel2ppcnt[panel],
								LastText:       "INVALID",
							})
							ui.log("Identified GenMon instance %v @ %v on screen %v", gid, panel, screen)
							//							fmt.Printf("GenMon added %v %v\n", screen, filename)
						}
					}
				}
			}
		}
		panel2ppcnt[panel] = numPlugins
	}
	for _, gm := range ui.GenMons {
		if ppc, ok := panel2ppcnt[gm.PanelID]; ok {
			gm.PanelPluginCnt = ppc
		}
	}

}

func (ui *XFCEGenMonUI) Init(inst *Inst) error {
	ui.inst = inst
	ui.runID = fmt.Sprintf("%v", time.Now().Unix())
	ui.log = logfunk.MakeF(ui.inst.Log, logfunk.Module("XFCEGenmon"))
	// ui.log = logfunk.MakeF(logfunk.SilentSink(), logfunk.Module("XFCEGenmon"))
	ui.iconDone = make(map[string]struct{})
	return nil
}

var firstupd = false

func (ui *XFCEGenMonUI) ScreenUpdate(wms WMState, events []Evt) {
	if len(events) == 0 {
		return
	}
	defer func() {
		firstupd = false
	}()
	if firstupd {
		_, _ = exec.Command("notify-send", fmt.Sprintf("shin_upd_start_%v", timestamp.Now().String())).Output()
	}
	for _, evt := range events {
		if evt.Type == EvtTypeScreenChange {
			ui.GenmonConfigDone = false
		}
	}
	if !ui.GenmonConfigDone {
		ui.GenmonConfigDone = true
		ui.DetectGenmonConfig(wms)
	}
	if firstupd {
		_, _ = exec.Command("notify-send", fmt.Sprintf("shin_upd_cfg_%v", timestamp.Now().String())).Output()
	}

	trim := func(t string, l int) string {
		if len(t) > l {
			return t[0:l-1] + "..."
		}
		return t
	}

	for _, screen := range wms.Screens {
		txt := ""
		iconText := ""
		tilerText := ""
		tags := append([]string{}, screen.Tags...)
		numWins := 0
		_ = tags
		_ = txt
		_ = iconText
		wins := []WMWindowState{}
		frontWin := ""
		sp := ui.inst.GetScreenParams(screen.ID, false)
		if sp.Tiler != nil {
			if n, ok := ui.inst.TilerNames[sp.Tiler]; ok {
				uc := ""
				for _, r := range n {
					if unicode.IsUpper(r) {
						uc = uc + string(r)
					}
				}
				if !sp.TilerAutomatic {
					uc = strings.ToLower(uc)
				}
				tilerText = uc
			} else {
				tilerText = "?"
			}
		} else {
			tilerText = "*"
		}

		actWinScreen := ""

		for _, win := range wms.Windows {
			if win.PrimaryScreen == screen.ID && !win.Panel && !win.Special && !win.IsDesktop && win.Visible {
				if screen.ActiveTag == "" || win.Tag == "" || screen.ActiveTag == win.Tag {
					numWins++
					wins = append(wins, win)
					frontWin = win.ID
					if win.Active {
						actWinScreen = win.PrimaryScreen
						if ui.showIcon {
							fn := fmt.Sprintf("/tmp/shingicon_%v_%v.png", ui.runID, win.ID)
							if _, ok := ui.iconDone[fn]; ok {
								iconText = "<img>" + fn + "</img>"
							} else {
								icn := ui.inst.WMBE.Icon(win.ID)
								if icn != nil {
									w, err := os.Create(fn)
									if w != nil {
										ui.iconDone[fn] = struct{}{}
										png.Encode(w, icn)
										w.Close()
										ui.log("Written %v", fn)
										iconText = "<img>" + fn + "</img>"
									} else {
										ui.log("Writing %v failed: %v", fn, err)
									}
								}
							}
						}
					}
				}
			}
		}

		if actWinScreen == "" {
		}

		if iconText == "" && ui.showIcon {
			fn := fmt.Sprintf("/tmp/shingicon_shin.png")
			if _, ok := ui.iconDone[fn]; ok {
				iconText = "<img>" + fn + "</img>"
			} else {
				icn := ui.inst.GetShinIcon(16)
				if icn != nil {
					w, err := os.Create(fn)
					if w != nil {
						ui.iconDone[fn] = struct{}{}
						png.Encode(w, icn)
						w.Close()
						ui.log("Written %v", fn)
						iconText = "<img>" + fn + "</img>"
					} else {
						ui.log("Writing %v failed: %v", fn, err)
					}
				}
			}
		}

		tagText := ""
		if len(tags) > 0 {
			for _, tn := range tags {
				if tn == screen.ActiveTag {
					if screen.ID == actWinScreen {
						tagText += "<span " + ui.styleActive + "> " + tn + " </span>"
					} else {
						tagText += "<span " + ui.styleInactiveFront + "> " + tn + " </span>"
					}
				} else {
					tagText += "<span " + ui.styleInactive + "> " + tn + " </span>"
				}
			}
			tagText += " "
		}

		//		first := true
		sort.Slice(wins, func(i, j int) bool { return wins[i].ID < wins[j].ID })

		for _, gm := range ui.GenMons {
			if strings.ToLower(gm.Screen) == strings.ToLower(screen.ID) && gm.PanelID != "" {

				panelEms := screen.Geometry.W / gm.PanelSize * 3

				emsavail := panelEms - len(tags)*3
				if gm.PanelPluginCnt > 1 {
					emsavail -= (gm.PanelPluginCnt - 1) * 8
				}

				txt = ""
				if emsavail > 50 && iconText != "" && ui.showIcon {
					txt += iconText
					emsavail -= 3
				}

				if emsavail < 30 {
					for _, win := range wins {
						if win.Active {
							txt += "<txt> " + tagText + trim(win.Title, 20) + "</txt>"
						}

					}
				} else {
					if true {
						if len(wins) > 1 {
							emsavail -= (len(wins) - 1) * 2
						}
						perWinBudget := 1
						if len(wins) > 0 {
							perWinBudget = emsavail / len(wins)
						}
						classBudget := perWinBudget * 4 / 10
						titleBudget := perWinBudget - classBudget

						if perWinBudget < 20 {
							titleBudget = 0
							classBudget = perWinBudget
						}
						txt += "<txt> " + tilerText + " " + tagText
						if ui.showWindows {
							first := true
							for _, win := range wins {
								if first {
									first = false
								} else {
									txt += "|"
								}
								tagMark := ""
								if win.Tag == "" && screen.ActiveTag != "" {
									tagMark = "! "
								}
								//							tagMark += "T(" + win.Tag + ")"
								if win.Active {
									if titleBudget > 0 {
										txt += "<span " + ui.styleActive + "> " + tagMark + trim(win.Class, classBudget-1) + " : "
										txt += trim(win.Title, titleBudget-1) + " </span>"
									} else {
										txt += "<span " + ui.styleActive + "> " + tagMark + trim(win.Class, classBudget-1) + " </span> "
									}
								} else {

									if win.ID == frontWin {
										if titleBudget > 0 {
											txt += "<span " + ui.styleInactiveFront + "> " + tagMark + trim(win.Class, classBudget-1) + " : " + trim(win.Title, titleBudget-1) + " </span>"
										} else {
											txt += "<span " + ui.styleInactiveFront + "> " + tagMark + trim(win.Class, classBudget) + " </span>"
										}
									} else {
										if titleBudget > 0 {
											txt += "<span " + ui.styleInactive + "> " + tagMark + trim(win.Class, classBudget-1) + " : " + trim(win.Title, titleBudget-1) + " </span>"
										} else {
											txt += "<span " + ui.styleInactive + "> " + tagMark + trim(win.Class, classBudget) + " </span>"
										}
									}
								}
							}
						}
						txt += "</txt>"
					} else {
						txt = "<txt></txt>"
					}
				}

				if txt != gm.LastText {
					if os.WriteFile(gm.Filename, []byte(txt), os.ModePerm) == nil {
						gm.LastText = txt
						err := exec.Command("xfce4-panel", fmt.Sprintf("--plugin-event=genmon-%v:refresh:bool:true", gm.PluginID)).Run()
						if err == nil {
							ui.log("updated %v", gm.Filename)
						} else {
							ui.log("updated %v (%v)", gm.Filename, err)
						}
					}
				}
			}
		}

		/*
			for _, win := range wins {
				if win.PrimaryScreen == screen.ID && !win.Panel && !win.Special && win.Visible {
					if screen.ActiveTag == "" || win.Tag == "" || screen.ActiveTag == win.Tag {
						//txt = txt + "<txt>"
						if first {
						} else {
							txt = txt + " "
						}
						cls := win.Class
						if maxTitleLen < 20 {
							if len(cls) > maxTitleLen {
								cls = cls[:maxTitleLen]
							}
						}
						if win.Active {
							txt = txt + "<span fgcolor='#000000' bgcolor='#a0a0a0'> "
							txt = txt + cls
							txt = txt + "</span><span fgcolor='#505050' bgcolor='#a0a0a0'>"
						} else {
							txt = txt + "<span fgcolor='#d0d0d0' > "
							txt = txt + cls
							txt = txt + "</span><span fgcolor='#b0b0b0' >"
						}
						if maxTitleLen > 1 {
							title := ":" + win.Title
							if len(title) > maxTitleLen {
								title = title[:(maxTitleLen-1)] + "..."
							}
							txt = txt + title
						}

						txt = txt + " </span>"
						first = false

					}
				}
			}
			txt += "</txt>"
			//		txt = txt + "<click>sh /home/carsten/genmon.sh</click>"

			for _, gm := range ui.GenMons {
				if strings.ToLower(gm.Screen) == strings.ToLower(screen.ID) {
					//				fmt.Printf("GMUpd %v %v %v %v\n", gm.PluginID, gm.Screen, screen.ID, gm.Filename)
					os.WriteFile(gm.Filename, []byte(txt), os.ModePerm)
				}
			}
		*/
	}
	for _, gm := range ui.GenMons {
		exec.Command("xfce4-panel", fmt.Sprintf("--plugin-event=genmon-%v:refresh:bool:true", gm.PluginID)).Run()
	}
	if firstupd {
		_, _ = exec.Command("notify-send", fmt.Sprintf("shin_upd_done_%v", timestamp.Now().String())).Output()
	}
}
