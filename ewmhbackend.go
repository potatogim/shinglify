package shinglify

import (
	"encoding/binary"
	"errors"
	"fmt"
	"image"
	"math"
	"os"
	"os/signal"
	"sort"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"syscall"
	"time"

	"git.pixeltamer.net/gopub/logfunk"
	"github.com/BurntSushi/xgb/randr"
	"github.com/BurntSushi/xgb/xinerama"
	"github.com/BurntSushi/xgb/xproto"
	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/ewmh"
	"github.com/BurntSushi/xgbutil/icccm"
	"github.com/BurntSushi/xgbutil/keybind"
	"github.com/BurntSushi/xgbutil/motif"
	"github.com/BurntSushi/xgbutil/xevent"
	"github.com/BurntSushi/xgbutil/xgraphics"
	"github.com/BurntSushi/xgbutil/xprop"
	"github.com/BurntSushi/xgbutil/xwindow"
)

type EWMHBackend struct {
	inst            *Inst
	state           WMState
	icons           map[string]image.Image
	lock            sync.Mutex
	evts            chan Evt
	mainCon         *xgbutil.XUtil
	screenChangeCon *xgbutil.XUtil
	log             logfunk.F
	quitting        int64
	updateSchedule  int32
	updateScheduleF int32
	updateScheduleG int32
	staleState      int32
	moveLockout     time.Time
}

func (e *EWMHBackend) Compatible() bool {
	return true
}

func (e *EWMHBackend) Init(inst *Inst) error {
	if e.mainCon != nil {
		return errors.New("double init")
	}
	e.quitting = 0
	e.updateSchedule = 0
	e.updateScheduleF = 0
	e.staleState = 0
	e.inst = inst
	e.icons = make(map[string]image.Image)
	var err error
	//e.log = logfunk.MakeF(e.inst.Log)
	e.log = logfunk.MakeF(logfunk.SilentSink())
	e.evts = make(chan Evt, 200)
	e.staleState = 1
	mkCon := func(withXLoop bool) (*xgbutil.XUtil, error) {
		c, err := xgbutil.NewConn()
		if err != nil {
			return nil, err
		}

		err = xinerama.Init(c.Conn())
		if err != nil {
			c.Conn().Close()
			return nil, err
		}
		err = randr.Init(c.Conn())
		if err != nil {
			c.Conn().Close()
			return nil, err
		}
		keybind.Initialize(c)
		if withXLoop {
			go xevent.Main(c)
		}
		return c, nil
	}
	e.mainCon, err = mkCon(true)
	if err != nil {
		return err
	}
	e.screenChangeCon, err = mkCon(false)
	if err != nil {
		return err
	}

	err = e.refresh(true, false)
	if err != nil {
		fmt.Printf("Refresh err: %v\n", err)
	}
	if err != nil {
		return err
	}
	go e.worker()
	go func() {
		csig := make(chan os.Signal, 2)
		signal.Notify(csig, os.Interrupt, syscall.SIGTERM)
		<-csig
		e.evts <- Evt{Timestamp: time.Now(), Type: EvtTypeQuit}
		e.Shut()
	}()

	return nil
}

func (e *EWMHBackend) Shut() {
	if !atomic.CompareAndSwapInt64(&e.quitting, 0, 1) {
		return // already done
	}
	if e.mainCon != nil {
		con := e.mainCon
		keybind.Detach(con, con.RootWin())
		close(e.evts)
	}
	e.mainCon = nil
}

func (e *EWMHBackend) generateWindowEvents(oldS, newS WMState, geomcheck bool) {
	tnow := time.Now()
	ow := make(map[string]*WMWindowState)
	nw := make(map[string]*WMWindowState)
	for i := range oldS.Windows {
		w := &oldS.Windows[i]
		ow[w.ID] = w
	}
	for i := range newS.Windows {
		w := &newS.Windows[i]
		nw[w.ID] = w
	}
	for wid := range nw {
		if _, ok := ow[wid]; !ok {
			e.evts <- Evt{Timestamp: tnow, Type: EvtTypeWindowOpen, Data: wid}
		}
	}
	for wid := range ow {
		if _, ok := nw[wid]; !ok {
			e.evts <- Evt{Timestamp: tnow, Type: EvtTypeWindowClose, Data: wid}
		}
	}
	for i := range newS.Windows {
		wid := newS.Windows[i].ID
		olds, oldok := ow[wid]
		news, newok := nw[wid]
		if oldok && newok {
			if !olds.Active && news.Active {
				e.evts <- Evt{Timestamp: tnow, Type: EvtTypeWindowActivate, Data: wid}
			}
			if geomcheck {
				if tnow.After(e.moveLockout) {
					if olds.Geometry != news.Geometry {
						e.evts <- Evt{Timestamp: tnow, Type: EvtTypeWindowGeometry, Data: wid}
					}
				}
			}
			if olds.Title != news.Title {
				e.evts <- Evt{Timestamp: tnow, Type: EvtTypeWindowTitle, Data: wid}
			}
		}
	}
}

func (e *EWMHBackend) ignMoves() {
	e.moveLockout = time.Now().Add(time.Millisecond * 200)
}

func (e *EWMHBackend) parseEDID(edid []byte) (manufacturer, product, serial string) {
	if len(edid) < 16 {
		return
	}
	manufacturer = fmt.Sprintf("%c%c%c", (edid[8]>>2&0x1f)+'A'-1, (((edid[8]&0x3)<<3)|((edid[9]&0xe0)>>5))+'A'-1, (edid[9]&0x1f)+'A'-1)
	product = fmt.Sprintf("%v", binary.LittleEndian.Uint16(edid[10:12]))
	serial = fmt.Sprintf("%v", binary.LittleEndian.Uint32(edid[12:16]))
	if len(edid) >= 128 {
		for ofs := 54; ofs <= 108; ofs += 18 {
			if edid[ofs+0] == 0 && edid[ofs+1] == 0 { // display descriptor
				if edid[ofs+3] == 0xff { //serial
					serial = strings.TrimSpace(string(edid[ofs+5 : ofs+18]))
				}
				if edid[ofs+3] == 0xfc { //product
					product = strings.TrimSpace(string(edid[ofs+5 : ofs+18]))
				}
			}
		}
	}
	return
}

func (e *EWMHBackend) Icon(wid WindowID) image.Image {
	e.lock.Lock()
	defer e.lock.Unlock()
	wnd := e.state.GetWindow(wid)
	if wnd != nil {
		if img, ok := e.icons[wnd.IconID]; ok {
			return img
		}
	}
	return nil
}

func (e *EWMHBackend) refresh(full bool, geomcheck bool) error {

	e.lock.Lock()
	defer e.lock.Unlock()
	//rlog := logfunk.MakeF(e.log)
	//fmt.Printf("refresh %v\n", full)

	newState := WMState{}

	//	var screenErr error
	//	var windowErr error

	con := e.mainCon
	var err error

	defer func() {
		if err != nil {
			fmt.Printf("Err: %v\n", err)
		}
	}()

	//	fmt.Printf("refresh2 %v\n", full)
	newState.WMName, err = ewmh.GetEwmhWM(con)

	// fmt.Printf("WMName: %v\n", newState.WMName)

	if strings.Contains(strings.ToLower(newState.WMName), "gnome shell") {
		e.inst.GnomeShellHack = true
	}
	//	fmt.Printf("refresh3 %v\n", full)

	desktopCnt, err := ewmh.NumberOfDesktopsGet(con)
	//	fmt.Printf("refresh4 %v\n", full)

	desktopCur, err := ewmh.CurrentDesktopGet(con)

	desktopNames, err := ewmh.DesktopNamesGet(con)

	desktopAreas, err := ewmh.WorkareaGet(con)

	validWndIDs := make(map[WindowID]struct{})

	if len(desktopNames) >= int(desktopCnt) && len(desktopAreas) >= int(desktopCnt) {
		for i := 0; i < int(desktopCnt); i++ {
			da := desktopAreas[i]
			newState.Desktops = append(newState.Desktops, WMDesktopState{ID: fmt.Sprintf("%v", i), Name: desktopNames[i], Geometry: Rct{X: da.X, Y: da.Y, W: int(da.Width), H: int(da.Height)}, Active: i == int(desktopCur)})
		}
	}

	actwndid, err := ewmh.ActiveWindowGet(con)
	if err != nil {
		return err
	}
	//	fmt.Printf("refresh5 %v\n", full)

	oldWinList := append(WMWindowStateList{}, e.state.Windows...)
	_ = oldWinList

	clients, err := ewmh.ClientListStackingGet(con)

	//	e.log("Read %v windows ------------------------", len(clients))

	//fmt.Printf("\n\n\nrefresh5.1 %v %v\n", full, len(clients))
	skipped := 0
	for _, client := range clients {
		wnd := WMWindowState{}
		if client == actwndid {
			wnd.Active = true
		}

		wnd.ID = WindowID(fmt.Sprintf("%v", client))

		validWndIDs[wnd.ID] = struct{}{}

		xwnd := xwindow.New(con, client)
		if xwnd == nil {
			continue
		}
		{
			wgeo, _ := xwnd.DecorGeometry()
			if wgeo != nil {
				wnd.Geometry = MkRct(wgeo.X(), wgeo.Y(), wgeo.Width(), wgeo.Height())
			}
		}

		changed := true
		for _, haveWnd := range oldWinList {
			if haveWnd.ID == wnd.ID {
				if haveWnd.Geometry == wnd.Geometry && haveWnd.Active == wnd.Active {
					skipped++
					//changed = false
					wnd = haveWnd
				}
				wnd.Tag = haveWnd.Tag
				wnd.IconID = haveWnd.IconID
			}
		}

		if wnd.IconID == "" {
			wnd.IconID = "icn" + wnd.ID
			xicon, err := xgraphics.FindIcon(con, client, 16, 16)
			if err == nil {
				w := xicon.Rect.Dx()
				h := xicon.Rect.Dy()
				dstimg := image.NewRGBA(image.Rect(0, 0, w, h))
				for y := 0; y < h; y++ {
					for x := 0; x < h; x++ {
						c := xicon.At(x, y)
						dstimg.Set(x, y, c)
					}
				}
				//				xgraphics.Blend(xicon, dstimg, image.ZP)
				e.icons[wnd.IconID] = dstimg
			}
		}

		if !changed {
		} else {

			wnd.Types, _ = ewmh.WmWindowTypeGet(con, client)

			dskidx, err := ewmh.WmDesktopGet(con, client)
			if err == nil && dskidx < math.MaxInt32 {
				wnd.Desktop = fmt.Sprintf("%v", dskidx)
			} else {
				wnd.Desktop = ""
			}

			wcls, err := icccm.WmClassGet(con, client)
			if err == nil && wcls != nil {
				wnd.Class = wcls.Class
				wnd.Instance = wcls.Instance
			}

			wname, err := ewmh.WmNameGet(con, client)
			if err != nil || wname == "" {
				wname, err = icccm.WmNameGet(con, client)
				if err != nil || wname == "" {
					wname = "N/A"
				}
			}

			wstate, _ := ewmh.WmStateGet(con, client)
			wnd.States = wstate

			var opaqueRct, frameRct, innerRct, reportRct Rct

			if opaques, err := ewmh.WmOpaqueRegionGet(con, client); err == nil {
				x0 := 100000
				x1 := -100000
				y0 := 100000
				y1 := -100000
				for _, o := range opaques {
					if o.X < x0 {
						x0 = o.X
					}
					if o.Y < y0 {
						y0 = o.Y
					}
					if o.X+int(o.Width) > x1 {
						x1 = o.X + int(o.Width)
					}
					if o.Y+int(o.Height) > y1 {
						y1 = o.Y + int(o.Height)
					}
					//					e.log("- O %v %v %v %v", o.X, o.Y, o.Width, o.Height)
				}
				opaqueRct = MkRct(x0, y0, x1-x0, y1-y0)
			}

			if f, err := xwnd.Geometry(); err == nil && f != nil {
				reportRct = MkRct(f.X(), f.Y(), f.Width(), f.Height())
				_ = reportRct
			}
			if f, err := ewmh.FrameExtentsGet(con, client); err == nil && f != nil {
				frameRct = MkRct(f.Left, f.Top, f.Right, f.Bottom)
			}
			var GtkFrameRct Rct
			{
				raw, err := xprop.PropValNums(xprop.GetProperty(con, client,
					"_GTK_FRAME_EXTENTS"))
				if err == nil {
					if len(raw) >= 4 {
						GtkFrameRct.X = int(raw[0])
						GtkFrameRct.Y = int(raw[1])
						GtkFrameRct.W = int(raw[2])
						GtkFrameRct.H = int(raw[3])
					}
				}
			}
			{
				gpr, err := xprop.GetProperty(con, client, "_SHINGLIFY_WINTAG")
				if err == nil {
					wnd.Tag = string(gpr.Value)
				}
			}
			if i, err := xwindow.RawGeometry(con, xproto.Drawable(client)); err == nil {
				innerRct = MkRct(i.X(), i.Y(), i.Width(), i.Height())
			}
			motifhints, err := motif.WmHintsGet(con, client)
			motifDeco := 0
			if motifhints != nil {
				if motif.Decor(motifhints) {
					motifDeco = 1
				}
				//				motifDeco = int(motifhints.Decoration)
			}
			/*


				trans, err := xproto.TranslateCoordinates(con.Conn(), client, con.RootWin(), 0, 0).Reply()
				transX := 0
				transY := 0
				if trans != nil {
					transX = int(trans.DstX) - wnd.Geometry.X
					transY = int(trans.DstY) - wnd.Geometry.Y
				} else {
					transX = -1
					transY = -1
				}*/

			//fmt.Printf("Wnd %v  DG %5v  IG %5v  FR %5v  XY %4v,%4v   MH %v/%v\n", (wname + "                         ")[:15], wnd.Geometry, wnd.InnerGeometry, wframe, transX, transY, motifhints, motifDecor)
			//			fmt.Printf("WFND %v  %v\n", wname, wstate)

			wtype := ""
			if len(wnd.Types) > 0 {
				wtype = wnd.Types[0]
			}

			wnd.Title = wname
			wnd.Special = true
			if len(wnd.Types) == 0 { // some applications (urxvt) do not set any types
				wnd.Special = false
			}
			for _, wndt := range wnd.Types {
				if strings.HasSuffix(wndt, "_TYPE_NORMAL") {
					wnd.Special = false
				}
			}
			wnd.Visible = true
			wnd.Maximized = false
			for _, st := range wstate {
				if strings.Contains(st, "_WM_SKIP_") {
					wnd.Special = true
				}
				if strings.Contains(st, "_TASKBAR") {
					wnd.Panel = true
				}
				if strings.HasSuffix(st, "_STATE_HIDDEN") {
					wnd.Visible = false
				}
				if strings.HasSuffix(st, "_MAXIMIZED_VERT") || strings.HasSuffix(st, "_MAXIMIZED_HORZ") {
					wnd.Maximized = true
				}
			}
			if strings.HasSuffix(wtype, "_DESKTOP") {
				wnd.Panel = false
				wnd.Special = true
				wnd.IsDesktop = true
			}
			if strings.HasSuffix(wtype, "_DOCK") {
				wnd.Panel = true
				wnd.Special = true
			}

			if !wnd.Special && !wnd.Panel {

				var innerBrd Rct
				innerBrd.X = innerRct.X
				innerBrd.Y = innerRct.Y
				innerBrd.W = wnd.Geometry.W - (innerRct.X + innerRct.W)
				innerBrd.H = wnd.Geometry.H - (innerRct.Y + innerRct.H)
				var posOfs Pnt
				posOfs.X = opaqueRct.X
				posOfs.Y = opaqueRct.Y
				var sizeOfs Pnt
				_ = sizeOfs

				var singleChildRct Rct

				sizeOfs.X = frameRct.X + frameRct.W
				sizeOfs.Y = frameRct.Y + frameRct.H

				wnd.SizeOfs = sizeOfs

				if false {
					if xtree, err := xproto.QueryTree(con.Conn(), client).Reply(); err == nil {
						fmt.Printf("Wnd %v C:%v G:%v  P:%v/%v/%v\n - F: %v IR: %v IB: %v\n",
							wnd.Title, len(xtree.Children), wnd.Geometry, xtree.Parent, xtree.Root, con.RootWin(),
							frameRct, innerRct, innerBrd)
						_ = xtree
						for _, cw := range xtree.Children {
							cwtypes, _ := ewmh.WmWindowTypeGet(con, cw)

							cwcls, err := icccm.WmClassGet(con, cw)
							if err == nil && cwcls != nil {
							}

							cwname, err := ewmh.WmNameGet(con, cw)
							if err != nil || wname == "" {
								cwname, err = icccm.WmNameGet(con, cw)
								if err != nil || wname == "" {
									cwname = "N/A"
								}
							}

							cwstate, _ := ewmh.WmStateGet(con, cw)

							var cwrct Rct
							xwnd := xwindow.New(con, client)
							if xwnd != nil {
								wgeo, _ := xwnd.Geometry()
								if wgeo != nil {
									cwrct = Rct{X: wgeo.X(), Y: wgeo.Y(), W: wgeo.Width(), H: wgeo.Height()}
								}
							}
							//						ewmh.Wm
							singleChildRct = cwrct
							fmt.Printf("    %v  | %v | %v |  %v  %v  %v  %v\n", cw, cwrct, wnd.Geometry, cwname, cwcls, cwstate, cwtypes)
						}
					}
				}

				_ = singleChildRct

				//				edidProp, err := randr.GetOutputProperty(scon.Conn(), xscreen, edidAtom, xproto.GetPropertyTypeAny, 0, 128, false, false).Reply()

				//				xproto.GetProperty()

				wnd.PosOfs.X = 0
				wnd.PosOfs.Y = 0
				if opaqueRct.Valid() && frameRct.IsZero() {
					wnd.PosOfs.X = -opaqueRct.X
					wnd.PosOfs.Y = -opaqueRct.Y
					wnd.SizeOfs.X = -(wnd.Geometry.W - opaqueRct.W)
					wnd.SizeOfs.Y = -(wnd.Geometry.H - opaqueRct.H)
					wnd.Geometry.X += opaqueRct.X
					wnd.Geometry.Y += opaqueRct.Y
					wnd.Geometry.W = opaqueRct.W
					wnd.Geometry.H = opaqueRct.H
				}
				if motifDeco != 0 {
					wnd.PosOfs.Y = frameRct.Y
				}
				if !GtkFrameRct.IsZero() {
					wnd.PosOfs.X += -GtkFrameRct.X
					wnd.PosOfs.Y += -GtkFrameRct.Y
					wnd.SizeOfs.X -= GtkFrameRct.W + GtkFrameRct.X
					wnd.SizeOfs.Y -= GtkFrameRct.H + GtkFrameRct.Y
					//	wnd.PosOfs.X -= GtkFrameRct.X
					//	wnd.PosOfs.Y -= GtkFrameRct.Y
				}
				if opaqueRct.Valid() && !frameRct.IsZero() {
					//					sizeOfs.X = (frameRct.X + frameRct.W)
					//					sizeOfs.Y = (frameRct.Y + frameRct.H)
					//					wnd.SizeOfs = sizeOfs
					//					wnd.Geometry.W -= frameRct.X + frameRct.W
					//					wnd.Geometry.H -= frameRct.Y + frameRct.H
				}
				//e.inst.Log("Wnd Cls %v S %v T %v  M %v\n   Geo %v Opq %v \n   Frm %v Inn %v\n   Gtk %v \n   Nam %v Diff %v,%v", (wnd.Class + "        ")[0:8], wstate, wtype, motifDeco, wnd.Geometry, opaqueRct, frameRct, innerRct, GtkFrameRct, (wname + "        ")[0:8], wnd.Geometry.W-innerRct.W, wnd.Geometry.H-innerRct.H)
				//				e.log("- InB %v  - POfs %v  - SOfs %v", innerBrd, posOfs, sizeOfs)

			}

		}
		newState.Windows = append(newState.Windows, wnd)
	}
	//	fmt.Printf("refresh6 %v %v\n", full, skipped)

	if full {
		{
			con := e.mainCon

			aname := "_SHINGLIFY_SCREENTAGS"
			tagsAtomR, err := xproto.InternAtom(con.Conn(), true, uint16(len(aname)), aname).Reply()
			tagsAtom := tagsAtomR.Atom
			_ = tagsAtom
			aname = "EDID"
			edidAtomR, err := xproto.InternAtom(con.Conn(), true, uint16(len(aname)), aname).Reply()
			edidAtom := edidAtomR.Atom
			aname = "EDID_DATA"
			edidDataAtomR, err := xproto.InternAtom(con.Conn(), true, uint16(len(aname)), aname).Reply()
			edidDataAtom := edidDataAtomR.Atom
			xscreens, err := randr.GetScreenResources(con.Conn(), con.RootWin()).Reply()
			if err != nil {
				return err
			}

			for _, xscreen := range xscreens.Outputs {
				scon := con

				inf, err := randr.GetOutputInfo(scon.Conn(), xscreen, 0).Reply()
				if err != nil {
					return err
				}
				var haveScreen WMScreenState
				haveTags := ""
				haveTagsSet := false
				for _, olds := range e.state.Screens {
					if olds.ID == ScreenID(inf.Name) {
						haveTags = strings.Join(olds.Tags, "\n")
						haveTagsSet = true
						haveScreen = olds
					}
				}
				_ = haveTags
				_ = haveTagsSet
				/*
					propTagsVal := ""
					tagsProp, err := randr.GetOutputProperty(scon.Conn(), xscreen, tagsAtom, xproto.GetPropertyTypeAny, 0, 128, false, false).Reply()
					if err == nil {
						if len(tagsProp.Data) > 0 {
							propTagsVal = string(tagsProp.Data)
							if !haveTagsSet {
								haveTags = propTagsVal
								haveScreen.Tags = strings.Split(haveTags, "\n")
								fmt.Printf("Found Screen Tags: %v\n", haveTags)
							}
						}
					}
					if propTagsVal!=haveTags && haveTagsSet{
						fmt.Printf("Need to update screen tags\n")

						typAtom, err := Atm(xu, typ)
						if err != nil {
							return err
						}

						randr.ChangeOutputProperty(scon.Conn(),xscreen,tagsAtom,typAtom,
					}
				*/
				devManufacturer := ""
				devProduct := ""
				devSerial := ""

				edidProp, err := randr.GetOutputProperty(scon.Conn(), xscreen, edidAtom, xproto.GetPropertyTypeAny, 0, 128, false, false).Reply()
				if err == nil {
					if len(edidProp.Data) >= 8 {
						devManufacturer, devProduct, devSerial = e.parseEDID(edidProp.Data)
					}
				}
				if devSerial == "" {
					edidDataProp, err := randr.GetOutputProperty(scon.Conn(), xscreen, edidDataAtom, xproto.GetPropertyTypeAny, 0, 128, false, false).Reply()
					if err == nil {
						if len(edidDataProp.Data) >= 8 {
							devManufacturer, devProduct, devSerial = e.parseEDID(edidProp.Data)
						}
					}
				}
				//				rlog("read screen %v crt", xscreen)
				var crt *randr.GetCrtcInfoReply
				if inf.Crtc != 0 {
					crt, err = randr.GetCrtcInfo(scon.Conn(), inf.Crtc, 0).Reply()
					if err != nil {
						return err
					}
				}
				if crt != nil {
					scrst := WMScreenState{
						ID:           ScreenID(inf.Name),
						Name:         string(inf.Name),
						Geometry:     Rct{X: int(crt.X), Y: int(crt.Y), W: int(crt.Width), H: int(crt.Height)},
						Manufacturer: devManufacturer,
						Product:      devProduct,
						Serial:       devSerial,
						Tags:         append([]Tag{}, haveScreen.Tags...),
						ActiveTag:    haveScreen.ActiveTag,
					}
					newState.Screens = append(newState.Screens, scrst)
					scrst.Tags = nil
					scrst.ActiveTag = ""
					newState.Outputs = append(newState.Outputs, scrst)
				} else {
					scrst := WMScreenState{
						ID:           ScreenID(inf.Name),
						Name:         string(inf.Name),
						Manufacturer: devManufacturer,
						Product:      devProduct,
						Serial:       devSerial,
					}
					if devProduct != "" || devSerial != "" || devManufacturer != "" {
						newState.Outputs = append(newState.Outputs, scrst)
					}
				}
			}
		}
		screenchange := false
		sort.Slice(e.state.Screens, func(i, j int) bool {
			return e.state.Screens[i].ID < e.state.Screens[j].ID
		})
		sort.Slice(e.state.Outputs, func(i, j int) bool {
			return e.state.Outputs[i].ID < e.state.Outputs[j].ID
		})
		if len(e.state.Screens) != len(newState.Screens) {
			screenchange = true
		} else {
			for i := range e.state.Screens {
				if e.state.Screens[i].Serial+e.state.Screens[i].ID != newState.Screens[i].Serial+newState.Screens[i].ID {
					screenchange = true
				}
			}
		}
		if screenchange {
			e.evts <- Evt{Timestamp: time.Now(), Type: EvtTypeScreenChange}
		}

	} else {
		newState.Screens = append([]WMScreenState{}, e.state.Screens...)

	}
	//	fmt.Printf("refresh7 %v\n", full)
	if err != nil {
		return err
	}
	for i := 0; i < len(newState.Windows); i++ {
		w := &newState.Windows[i]
		if w.Visible {
			w.VisibleGeometry.Add(w.Geometry)
			if !w.Special {
				for j := i + 1; j < len(newState.Windows); j++ {
					ow := newState.Windows[j]
					if ow.Visible {
						w.VisibleGeometry.Sub(ow.Geometry)
					}
				}
			}
		}
	}
	e.inst.PostProc(&newState)
	//	fmt.Printf("refresh8 %v\n", full)
	newState.ActiveWindowHistory = append([]WindowID{}, e.state.ActiveWindowHistory...)
	if len(newState.ActiveWindowHistory) == 0 {
		newState.ActiveWindowHistory = append(newState.ActiveWindowHistory, newState.GetActiveWindowID())
	} else {
		actwnd := newState.GetActiveWindowID()
		if newState.ActiveWindowHistory[len(newState.ActiveWindowHistory)-1] != actwnd {
			newState.ActiveWindowHistory = append(newState.ActiveWindowHistory, actwnd)
			for i := len(newState.ActiveWindowHistory) - 1; i >= 0; i-- {
				if _, ok := validWndIDs[newState.ActiveWindowHistory[i]]; !ok {
					newState.ActiveWindowHistory = append(newState.ActiveWindowHistory[:i], newState.ActiveWindowHistory[i+1:]...)
				}
			}
			if len(newState.ActiveWindowHistory) > 10 {
				newState.ActiveWindowHistory = newState.ActiveWindowHistory[1:]
			}
		}
	}

	e.generateWindowEvents(e.state, newState, geomcheck)

	e.state = newState
	//	fmt.Printf("refresh9 %v\n", full)

	atomic.StoreInt32(&e.staleState, 0)

	if true {
		for _, scr := range newState.Screens {
			e.log("S %v '%v': '%v' '%v'", scr.ID, scr.Geometry, scr.ActiveTag, scr.Tags)
		}
		for _, w := range e.state.Windows {
			e.log("W A:%v D:'%v' Tg:'%v'  Vis:%v Ti:%v - G:%v - VG:%v", w.Active, w.Desktop, w.Tag, w.Visible, w.Title, w.Geometry, w.VisibleGeometry)
		}
	}

	return nil
}

func (e *EWMHBackend) scheduleUpdate(full bool, geomcheck bool) {
	if geomcheck {
		atomic.StoreInt32(&e.updateScheduleG, 1)
	}
	old1 := atomic.SwapInt32(&e.updateSchedule, 1)
	if full {
		atomic.StoreInt32(&e.updateScheduleF, 1)
	}
	if old1 == 0 {
		go func() {
			//			fmt.Printf("Upd %v %v\n", full, geomcheck)
			time.Sleep(time.Millisecond * 50)
			f := atomic.LoadInt32(&e.updateScheduleF)
			g := atomic.LoadInt32(&e.updateScheduleG)
			atomic.StoreInt32(&e.updateSchedule, 0)
			atomic.StoreInt32(&e.updateScheduleF, 0)
			atomic.StoreInt32(&e.updateScheduleG, 0)
			e.refresh(f != 0, g != 0)
			atomic.StoreInt32(&e.staleState, 0)
		}()
	}
}

func (e *EWMHBackend) worker() {

	xMainwnd := xwindow.New(e.mainCon, e.mainCon.RootWin())

	_ = xMainwnd
	xMainwnd.Listen(xproto.EventMaskSubstructureNotify, xproto.EventMaskPropertyChange, xproto.EventMaskStructureNotify, xproto.EventMaskFocusChange /*,xproto.EventMaskButtonRelease  xproto.EventMaskButtonPress, xproto.EventMaskButtonRelease*/)
	/*
		xevent.ConfigureNotifyFun(func(x *xgbutil.XUtil, ev xevent.ConfigureNotifyEvent) {
			e.inst.Log("ConfigureEvt")
		}).Connect(e.mainCon, e.mainCon.RootWin())

		xevent.ClientMessageFun(func(x *xgbutil.XUtil, ev xevent.ClientMessageEvent) {
			e.inst.Log("ClientEvt")
		}).Connect(e.mainCon, e.mainCon.RootWin())
	*/
	xevent.HookFun(func(xu *xgbutil.XUtil, event interface{}) bool {
		if cne, ok := event.(xproto.ConfigureNotifyEvent); ok {
			_ = cne
			e.scheduleUpdate(false, true)
			//			e.inst.Log("Evt %v  %v   %v %v %v %v\n", cne.Window, cne.Event, cne.X, cne.Y, cne.Width, cne.Height)
		}
		//e.inst.Log("PropEvt1")
		return true
	}).Connect(e.mainCon)

	xevent.PropertyNotifyFun(func(x *xgbutil.XUtil, ev xevent.PropertyNotifyEvent) {
		chgName, _ := xprop.AtomName(e.mainCon, ev.Atom)
		//e.inst.Log("PropEvt2")
		//		fmt.Printf("Chg(%v) %v\n", chgName, ev)
		if chgName == "_NET_WORKAREA" {
			e.scheduleUpdate(true, false)
		} else {
			e.scheduleUpdate(false, false)
		}
	}).Connect(e.mainCon, e.mainCon.RootWin())
	/*
		if r, err := xproto.GetWindowAttributes(e.mainCon.Conn(), e.mainCon.RootWin()).Reply(); err == nil {
			e.inst.Log("%x %x", r.YourEventMask, r.AllEventMasks)
			err = xproto.ChangeWindowAttributesChecked(e.mainCon.Conn(), e.mainCon.RootWin(), xproto.CwEventMask,
				[]uint32{
					1 | 2 | xproto.EventMaskPointerMotion | xproto.EventMaskPropertyChange | xproto.EventMaskStructureNotify | xproto.EventMaskSubstructureNotify,
				}).Check()
			e.inst.Log("CMA %v", err)
			if r, err := xproto.GetWindowAttributes(e.mainCon.Conn(), e.mainCon.RootWin()).Reply(); err == nil {
				e.inst.Log("%x %x", r.YourEventMask, r.AllEventMasks)
			}
		}
	*/
	//	xproto.ChangeWindowAttributes(e.mainCon,e.mainCon.RootWin(),

	con := e.screenChangeCon
	_ = randr.SelectInputChecked(con.Conn(), con.RootWin(),
		randr.NotifyMaskScreenChange|
			randr.NotifyMaskCrtcChange|
			randr.NotifyMaskOutputChange|
			randr.NotifyMaskOutputProperty).Check()

	for {
		evt, err := con.Conn().WaitForEvent()
		//		fmt.Printf("worker %v %v\n", evt, err)
		if evt == nil && err == nil {
			e.evts <- Evt{Timestamp: time.Now(), Type: EvtTypeQuit}
			e.Shut()
			return
		}
		if err != nil {
			fmt.Printf("%v\n", err)
			e.evts <- Evt{Timestamp: time.Now(), Type: EvtTypeError, Data: err.Error()}
		}
		if evt != nil {
			e.evts <- Evt{Timestamp: time.Now(), Type: EvtTypeRaw, Params: evt}
			e.scheduleUpdate(false, false)
		}
	}
}

func (e *EWMHBackend) State(refresh bool) WMState {
	if refresh {
		e.invalidateState()
	}
	for atomic.LoadInt32(&e.staleState) != 0 {
		time.Sleep(time.Millisecond)
	}
	e.lock.Lock()
	r := e.state
	e.lock.Unlock()
	return r
}

func (e *EWMHBackend) invalidateState() {
	atomic.StoreInt32(&e.staleState, 1)
	e.scheduleUpdate(false, false)
}

func (e *EWMHBackend) win2x(wid WindowID) xproto.Window {
	v, err := strconv.Atoi(wid)
	if err != nil {
		return 0
	}
	return xproto.Window(v)
}

func (e *EWMHBackend) SetWindowTag(wid WindowID, tag Tag) error {
	e.lock.Lock()
	defer e.lock.Unlock()
	for i := range e.state.Windows {
		if e.state.Windows[i].ID == wid {
			if e.state.Windows[i].Tag != tag {
				e.inst.Log("ChgWndTag %v %v->%v", e.state.Windows[i].WindowInfo(), e.state.Windows[i].Tag, tag)
				e.state.Windows[i].Tag = tag
				xprop.ChangeProp(e.mainCon, e.win2x(e.state.Windows[i].ID), 8, "_SHINGLIFY_WINTAG", "UTF8_STRING", []byte(tag))
			}
		}
	}
	return nil
}

func (e *EWMHBackend) AddScreenTag(sid ScreenID, tag Tag) error {
	e.lock.Lock()
	defer e.lock.Unlock()
	for i := range e.state.Screens {
		if e.state.Screens[i].ID == sid {
			found := false
			for _, t := range e.state.Screens[i].Tags {
				if t == tag {
					found = true
				}
			}
			if !found {
				e.state.Screens[i].Tags = append(e.state.Screens[i].Tags, tag)
				sort.Strings(e.state.Screens[i].Tags)
			}
			if e.state.Screens[i].ActiveTag == "" {
				e.state.Screens[i].ActiveTag = tag
				for wi := range e.state.Windows {
					if e.state.Windows[wi].Tag == "" && e.state.Windows[wi].PrimaryScreen == sid {
						e.state.Windows[wi].Tag = tag
					}
				}
			}
		}
	}
	return nil
}

func (e *EWMHBackend) GetScreenTag(sid ScreenID) (string, error) {
	e.lock.Lock()
	defer e.lock.Unlock()
	for i := range e.state.Screens {
		if e.state.Screens[i].ID == sid {
			return e.state.Screens[i].ActiveTag, nil
		}
	}
	return "", fmt.Errorf("unknown screen %v", sid)
}

func (e *EWMHBackend) SetScreenTag(sid ScreenID, tag Tag) error {
	e.lock.Lock()
	defer e.lock.Unlock()
	for i := range e.state.Screens {
		if e.state.Screens[i].ID == sid {
			found := false
			for _, t := range e.state.Screens[i].Tags {
				if t == tag {
					found = true
				}
			}
			if !found {
				e.state.Screens[i].Tags = append(e.state.Screens[i].Tags, tag)
				sort.Strings(e.state.Screens[i].Tags)
			}
			e.state.Screens[i].ActiveTag = tag
		} else {
			for i, t := range e.state.Screens[i].Tags {
				if t == tag {
					e.state.Screens[i].Tags = append(e.state.Screens[i].Tags[:i], e.state.Screens[i].Tags[i+1:]...)
				}
			}
			if e.state.Screens[i].ActiveTag == tag {
				e.state.Screens[i].ActiveTag = ""
			}
		}
	}
	return nil
}

func (e *EWMHBackend) RemScreenTag(sid ScreenID, tag Tag) error {
	e.lock.Lock()
	defer e.lock.Unlock()
	for i := range e.state.Screens {
		if e.state.Screens[i].ID == sid {
			for i, t := range e.state.Screens[i].Tags {
				if t == tag {
					e.state.Screens[i].Tags = append(e.state.Screens[i].Tags[:i], e.state.Screens[i].Tags[i+1:]...)
				}
			}
			if e.state.Screens[i].ActiveTag == tag {
				e.state.Screens[i].ActiveTag = ""
			}
		}
	}
	return nil
}

func (e *EWMHBackend) Show(wid WindowID, show bool) error {
	e.ignMoves()
	xw := e.win2x(wid)
	con := e.mainCon

	st, err := ewmh.WmStateGet(con, xw)
	if err != nil {
		return err
	}

	idx := -1
	for i, s := range st {
		if s == "_NET_WM_STATE_HIDDEN" {
			idx = i
		}
	}
	if !show {
		if idx == -1 {
			st = append(st, "_NET_WM_STATE_HIDDEN")
			err = ewmh.WmStateSet(con, xw, st)
			err = ewmh.WmStateReq(con, xw, 2, "_NET_WM_STATE_HIDDEN")
		}
	} else {
		if idx != -1 {
			st = append(st[:idx], st[idx+1:]...)
			err = ewmh.WmStateSet(con, xw, st)
			err = ewmh.WmStateReq(con, xw, 2, "_NET_WM_STATE_HIDDEN")
		}
	}
	return err
}

func (e *EWMHBackend) Maximize(wid WindowID, dir int) error {
	e.ignMoves()
	con := e.mainCon
	wndi := e.state.FindWindowIndex(wid)
	xw := e.win2x(wid)
	if wndi == -1 {
		return nil
	}
	wnd := &e.state.Windows[wndi]
	clear := false
	maximized := false
	for _, s := range wnd.States {
		if s == "_NET_WM_STATE_MAXIMIZED_HORZ" || s == "_NET_WM_STATE_MAXIMIZED_VERT" {
			maximized = true
		}
	}
	if dir == 0 && maximized {
		ewmh.WmStateReq(con, xw, 0, "_NET_WM_STATE_MAXIMIZED_HORZ")
		ewmh.WmStateReq(con, xw, 0, "_NET_WM_STATE_MAXIMIZED_VERT")
		clear = true
	}
	if dir == 1 && !maximized {
		ewmh.WmStateReq(con, xw, 2, "_NET_WM_STATE_MAXIMIZED_HORZ")
		ewmh.WmStateReq(con, xw, 2, "_NET_WM_STATE_MAXIMIZED_VERT")
	}
	if dir == 2 {
		if maximized {
			ewmh.WmStateReq(con, xw, 0, "_NET_WM_STATE_MAXIMIZED_HORZ")
			ewmh.WmStateReq(con, xw, 0, "_NET_WM_STATE_MAXIMIZED_VERT")
			clear = true
		} else {
			ewmh.WmStateReq(con, xw, 2, "_NET_WM_STATE_MAXIMIZED_HORZ")
			ewmh.WmStateReq(con, xw, 2, "_NET_WM_STATE_MAXIMIZED_VERT")
		}
	}
	if clear {
		for i := len(wnd.States) - 1; i >= 0; i-- {
			if wnd.States[i] == "_NET_WM_STATE_MAXIMIZED_HORZ" || wnd.States[i] == "_NET_WM_STATE_MAXIMIZED_VERT" {
				wnd.States = append(wnd.States[:i], wnd.States[i+1:]...)
			}
		}
	}
	e.State(true)
	return nil
}

func (e *EWMHBackend) Move(wid WindowID, r Rct) error {
	e.ignMoves()
	xw := e.win2x(wid)
	con := e.mainCon

	xwnd := xwindow.New(con, xw)
	if xwnd == nil {
		return nil
	}

	e.lock.Lock()
	idx := e.state.FindWindowIndex(wid)
	if idx != -1 {
		//		e.state.Windows[idx].Geometry = r
	}
	e.lock.Unlock()

	e.Maximize(wid, 0)

	wnd := e.state.GetWindow(wid)
	if wnd != nil {
		r.X += wnd.PosOfs.X
		r.Y += wnd.PosOfs.Y
		r.W -= wnd.SizeOfs.X
		r.H -= wnd.SizeOfs.Y
	}

	err := ewmh.MoveresizeWindow(con, xw, r.X, r.Y, r.W, r.H)
	//err := xwnd.WMMoveResize(int(r.X), int(r.Y), int(r.W), int(r.H))
	return err
}

func (e *EWMHBackend) ArrangeWindows(stack []WindowID) error {
	e.ignMoves()
	con := e.mainCon
	xws := []xproto.Window{}
	for _, w := range stack {
		xws = append(xws, e.win2x(w))
	}
	err := ewmh.ClientListStackingSet(con, xws)
	return err
}

func (e *EWMHBackend) Activate(wid WindowID) error {
	e.ignMoves()
	var err error
	xw := e.win2x(wid)
	con := e.mainCon
	err = ewmh.ActiveWindowReq(con, xw)
	winw := 0
	winh := 0
	e.lock.Lock()
	widx := e.state.FindWindowIndex(wid)
	if widx != -1 {
		winw = e.state.Windows[widx].Geometry.W
		winh = e.state.Windows[widx].Geometry.H
	}
	e.lock.Unlock()

	xproto.WarpPointerChecked(con.Conn(), con.RootWin(), xw, 0, 0, 0, 0, int16(winw/2), int16(winh/2)).Reply()
	return err
}

func (e *EWMHBackend) BeginChange() error {
	return nil
}

func (e *EWMHBackend) EndChange() error {
	//	e.refresh(true)
	e.scheduleUpdate(false, false)
	return nil
}
func (e *EWMHBackend) RegisterKeyBind(keys KeyBindDef, evtDataPress interface{}, evtDataRelease interface{}) error {
	con := e.mainCon
	if _, _, err := keybind.ParseString(con, keys); err != nil {
		return err
	}
	if evtDataPress != nil {
		kh := keybind.KeyPressFun(
			func(X *xgbutil.XUtil, ev xevent.KeyPressEvent) {
				e.evts <- Evt{Timestamp: time.Now(), Type: EvtTypeKeyPress, Data: keys, Params: evtDataPress}
			})
		err := kh.Connect(con, con.RootWin(), string(keys), true)
		if err != nil {
			return err
		}
	}
	if evtDataRelease != nil {
		kh := keybind.KeyReleaseFun(
			func(X *xgbutil.XUtil, ev xevent.KeyReleaseEvent) {
				e.evts <- Evt{Timestamp: time.Now(), Type: EvtTypeKeyRelease, Data: keys, Params: evtDataRelease}
			})
		err := kh.Connect(con, con.RootWin(), string(keys), true)
		if err != nil {
			return err
		}
	}

	return nil
}

func (e *EWMHBackend) EvtChan() chan Evt {
	return e.evts
}
