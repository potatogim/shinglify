module gitlab.com/corthbandt/shinglify

go 1.17

require (
	git.pixeltamer.net/gopub/logfunk v1.0.11
	git.pixeltamer.net/gopub/timestamp v1.1.0 // indirect
	github.com/BurntSushi/xgb v0.0.0-20210121224620-deaf085860bc
	github.com/BurntSushi/xgbutil v0.0.0-20190907113008-ad855c713046
	github.com/yuin/gopher-lua v0.0.0-20210529063254-f4c35e4016d9
	github.com/BurntSushi/freetype-go v0.0.0-20160129220410-b763ddbfe298 // indirect
	github.com/BurntSushi/graphics-go v0.0.0-20160129215708-b43f31a4a966 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
)
