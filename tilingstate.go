package shinglify

import (
	lua "github.com/yuin/gopher-lua"
)

type Tag = string

type TagList = []Tag

type TilingMode string

type ScreenParams struct {
	ScreenID       ScreenID
	TagList        TagList
	ActiveTag      Tag
	Tiler          *lua.LFunction
	TilerEnable    bool
	TilerAutomatic bool
	GapOuter       int
	GapInner       int
	GapSingle      int
	TilerDX        int
	TilerDY        int
}

func (inst *Inst) GetScreenParams(scrid string, isolate bool) *ScreenParams {
	if scrid == "" {
		return inst.DefaultScreenParams
	}
	if sp, ok := inst.ScreenParamsMap[scrid]; ok {
		return sp
	}
	if isolate {
		sp := &ScreenParams{}
		*sp = *inst.DefaultScreenParams
		sp.ScreenID = scrid
		inst.ScreenParamsMap[scrid] = sp
		return sp
	}
	return inst.DefaultScreenParams
}

func (inst *Inst) GetTilerWindowIDList(scr *WMScreenState, wnd *WMWindowState) WMWindowIDList {
	ret := WMWindowIDList{}

	wms := inst.WMBE.State(false)

	for _, cnd := range wms.Windows {
		if !cnd.Special && !cnd.Panel && cnd.Visible {
			if cnd.PrimaryScreen == wnd.PrimaryScreen {
				//				fmt.Printf("Chk '%v' '%v' '%v' '%v'\n", cnd.ID, cnd.PrimaryScreen, cnd.Tag, cnd.Title)
				if wnd.Tag == "" || cnd.Tag == "" || wnd.Tag == cnd.Tag {
					ret = append(ret, cnd.ID)
				}
			}
		}
	}

	return ret
}
