package shinglify

import (
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"os"
	"os/exec"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	_ "embed"

	"git.pixeltamer.net/gopub/logfunk"
	"git.pixeltamer.net/gopub/timestamp"

	lua "github.com/yuin/gopher-lua"
)

//go:embed shinglify.lua
var DefaultConfig string

var VersionString string
var BuildDate string

type Inst struct {
	L             *lua.LState
	Log           logfunk.F
	WMBE          WMBackend
	ScriptSource  string
	Script        *lua.LFunction
	EvtType       string
	EvtData       string
	EvtParam      lua.LValue
	RetileLockout time.Time

	ScreenParamsMap     map[ScreenID]*ScreenParams // keeps track of current tag assignments
	WinFloat            map[WindowID]struct{}
	DefaultScreenParams *ScreenParams
	GnomeShellHack      bool

	TilerNames map[*lua.LFunction]string

	UIBackends []UIBackend

	pauseUpdates int

	ShinIconCache     map[int]image.Image
	ShinIconCacheLock sync.Mutex
}

func Main() {
	/*
		if _, err := os.Stat("/tmp/shinglify.lock"); err == nil {
			return
		}
		os.WriteFile("/tmp/shinglify.lock", []byte{1}, os.ModePerm)
		defer func() {
			os.Remove("/tmp/shinglify.lock")
		}()
	*/
	if false {
		_, _ = exec.Command("notify-send", fmt.Sprintf("shin_start_%v", timestamp.Now().String())).Output()
	}

	inst := Inst{}

	inst.Log = logfunk.MakeF(logfunk.ShortTime, logfunk.ShowTime)

	inst.Log("shin start")

	initTry := func() error {
		inst.WMBE = nil

		if inst.WMBE == nil {
			candidate := &EWMHBackend{}
			if candidate.Compatible() {
				inst.WMBE = candidate
			}
		}

		if inst.WMBE == nil {
			inst.Log(logfunk.Error, "No compatible backend found.\n")
			return errors.New("No compatible backend found")
		}

		err := inst.WMBE.Init(&inst)
		return err
	}

	var err error

	for retry := 1; retry <= 25; retry++ {
		inst.Log(logfunk.Error, "initializing backend: retry %v", retry)
		if retry > 5 {
			time.Sleep(time.Millisecond * 50)
		}
		if retry > 20 {
			time.Sleep(time.Millisecond * 200)
		}
		if err = initTry(); err != nil {
			inst.Log(logfunk.Error, "Error initializing backend: %v, retry %v", err, retry)
		} else {
			retry = 100
		}
	}

	if err != nil {
		inst.Log(logfunk.Error, "Error initializing backend: %v, giving up", err)
		return
	}

	inst.DefaultScreenParams = &ScreenParams{}
	inst.ScreenParamsMap = make(map[ScreenID]*ScreenParams)
	inst.WinFloat = make(map[WindowID]struct{})
	inst.TilerNames = make(map[*lua.LFunction]string)
	inst.ShinIconCache = make(map[int]image.Image)

	inst.L = lua.NewState()

	scriptNames := []string{
		"$HOME/.config/shinglify/shinglify.lua",
		"$HOME/.config/shinglify.lua",
		"$HOME/.shinglify.lua",
		"/etc/shinglify.lua",
	}
	scriptFile := ""

	for _, c := range scriptNames {
		candidate := os.ExpandEnv(c)
		content, err := os.ReadFile(candidate)
		if err == nil && len(content) > 0 {
			inst.Log("Using script %v", candidate)
			inst.ScriptSource = string(content)
			scriptFile = candidate
			break
		}
	}
	if len(inst.ScriptSource) == 0 {
		inst.ScriptSource = DefaultConfig
	}

	if len(inst.ScriptSource) == 0 {
		fmt.Printf("No script found, tried %v\n", strings.Join(scriptNames, " "))
		return
	}

	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "-h", "--help":
			fmt.Printf("Shinglify %v %v\n\n", VersionString, BuildDate)
			fmt.Printf("(c) 2021 Carsten Orthbandt carsten@pixeltamer.net\n\n")
			fmt.Printf("See https://gitlab.com/corthbandt/shinglify\n")
			fmt.Printf("shinglify --printconfig\n")
			fmt.Printf("  print the configuration used.\n")
			return
		case "--printconfig":
			fmt.Printf("%v\n", inst.ScriptSource)
			return
		case "--print-default-config":
			fmt.Printf("%v\n", DefaultConfig)
			return
		case "--print-screen-layout":
			wms := inst.WMBE.State(false)
			fmt.Printf("%v\n", wms.ScreenLayouts)
			for _, scr := range wms.Screens {
				fmt.Printf("%v: Geom:%v Product:%v\n", scr.ID, scr.Geometry, scr.Product)
			}
			return
		case "--outputs":
			inst.PrintOutputInfo()
			return
		case "--dump":
			wms := inst.WMBE.State(false)
			js, _ := json.MarshalIndent(wms, "", "  ")
			fmt.Printf("%v\n", string(js))
			return
		case "--rofi":
			inst.RofiWindowSwitcher()
			return
		default:
			return
		}
	}

	{
		inst.L.SetGlobal("log", inst.L.NewFunction(inst.LuaLog))

		inst.L.SetGlobal("pause_updates", inst.L.NewFunction(inst.LuaPauseUpdates))
		inst.L.SetGlobal("unpause_updates", inst.L.NewFunction(inst.LuaUnpauseUpdates))

		inst.L.SetGlobal("evt_type", inst.L.NewFunction(inst.LuaEvtType))
		inst.L.SetGlobal("evt_data", inst.L.NewFunction(inst.LuaEvtData))
		inst.L.SetGlobal("evt_param", inst.L.NewFunction(inst.LuaEvtParam))
		inst.L.SetGlobal("key_bind", inst.L.NewFunction(inst.LuaKeyBind))

		inst.L.SetGlobal("rofi_window_switcher", inst.L.NewFunction(inst.LuaRofiWindowSwitcher))

		inst.L.SetGlobal("get_wm_state", inst.L.NewFunction(inst.LuaGetWMState))

		inst.L.SetGlobal("get_active_windowid", inst.L.NewFunction(inst.LuaGetActiveWindowID))
		inst.L.SetGlobal("get_active_screenid", inst.L.NewFunction(inst.LuaGetActiveScreenID))
		inst.L.SetGlobal("get_window_screenid", inst.L.NewFunction(inst.LuaGetWindowScreenID))

		inst.L.SetGlobal("navigate_left", inst.L.NewFunction(inst.LuaNavigateLeft))
		inst.L.SetGlobal("navigate_right", inst.L.NewFunction(inst.LuaNavigateRight))
		inst.L.SetGlobal("navigate_up", inst.L.NewFunction(inst.LuaNavigateUp))
		inst.L.SetGlobal("navigate_down", inst.L.NewFunction(inst.LuaNavigateDown))
		inst.L.SetGlobal("navigate_next", inst.L.NewFunction(inst.LuaNavigateNext))
		inst.L.SetGlobal("navigate_prev", inst.L.NewFunction(inst.LuaNavigatePrev))
		inst.L.SetGlobal("navigate_next_on_screen", inst.L.NewFunction(inst.LuaNavigateNextOnScreen))
		inst.L.SetGlobal("navigate_prev_on_screen", inst.L.NewFunction(inst.LuaNavigatePrevOnScreen))
		inst.L.SetGlobal("navigate_next_on_tag", inst.L.NewFunction(inst.LuaNavigateNextOnTag))
		inst.L.SetGlobal("navigate_prev_on_tag", inst.L.NewFunction(inst.LuaNavigatePrevOnTag))
		inst.L.SetGlobal("navigate_next_tag_on_screen", inst.L.NewFunction(inst.LuaNavigateNextTagOnScreen))
		inst.L.SetGlobal("navigate_prev_tag_on_screen", inst.L.NewFunction(inst.LuaNavigatePrevTagOnScreen))
		inst.L.SetGlobal("move_window_left", inst.L.NewFunction(inst.LuaWindowMoveLeft))
		inst.L.SetGlobal("move_window_right", inst.L.NewFunction(inst.LuaWindowMoveRight))
		inst.L.SetGlobal("move_window_up", inst.L.NewFunction(inst.LuaWindowMoveUp))
		inst.L.SetGlobal("move_window_down", inst.L.NewFunction(inst.LuaWindowMoveDown))

		inst.L.SetGlobal("apply_tags", inst.L.NewFunction(inst.LuaApplyTags))
		inst.L.SetGlobal("set_active_window_tag", inst.L.NewFunction(inst.LuaSetActiveWindowTag))
		inst.L.SetGlobal("set_window_tag", inst.L.NewFunction(inst.LuaSetWindowTag))
		inst.L.SetGlobal("add_screen_tag", inst.L.NewFunction(inst.LuaAddScreenTag))
		//		inst.L.SetGlobal("release_empty_screen_tags", inst.L.NewFunction(inst.LuaReleaseEmptyScreenTags))
		inst.L.SetGlobal("show_tag", inst.L.NewFunction(inst.LuaShowTag))
		inst.L.SetGlobal("tag_visible", inst.L.NewFunction(inst.LuaTagVisible))

		inst.L.SetGlobal("show_window", inst.L.NewFunction(inst.LuaShowWindow))
		inst.L.SetGlobal("maximize_window", inst.L.NewFunction(inst.LuaMaximizeWindow))
		inst.L.SetGlobal("reposition_window", inst.L.NewFunction(inst.LuaRepositionWindow))

		inst.L.SetGlobal("run_tiler", inst.L.NewFunction(inst.LuaRunTiler))
		inst.L.SetGlobal("tiler_helper_get_screen_layout_params", inst.L.NewFunction(inst.LuaTilerHelperGetScreenLayoutParams))
		inst.L.SetGlobal("set_tiler", inst.L.NewFunction(inst.LuaSetTiler))
		inst.L.SetGlobal("get_tiler", inst.L.NewFunction(inst.LuaGetTiler))
		inst.L.SetGlobal("enable_tiler", inst.L.NewFunction(inst.LuaEnableTiler))
		inst.L.SetGlobal("set_gaps", inst.L.NewFunction(inst.LuaSetGaps))
		inst.L.SetGlobal("resize_tiler", inst.L.NewFunction(inst.LuaResizeTiler))

		inst.L.SetGlobal("enableui_xfcegenmon", inst.L.NewFunction(inst.LuaEnableUIXFCEGenMon))

		inst.L.SetGlobal("get_screen_layout", inst.L.NewFunction(inst.LuaGetScreenLayout))
		inst.L.SetGlobal("match_screen_layout", inst.L.NewFunction(inst.LuaMatchScreenLayout))
		inst.L.SetGlobal("move_xfcepanel_to_screen", inst.L.NewFunction(inst.LuaMoveXFCEPanelToScreen))

		inst.RegisterTilers()
	}

	inst.Script, err = inst.L.LoadString(inst.ScriptSource)
	if err != nil {
		fmt.Printf("Error initializing script %v: %v\n", scriptFile, err)
		return
	}

	inst.WMBE.State(false)
	inst.WMBE.EvtChan() <- Evt{Timestamp: time.Now(), Type: EvtTypeInit}

	/*
		inst.EvtType = "Init"

		{
			inst.L.Push(inst.Script)
			err = inst.L.PCall(0, lua.MultRet, nil)
			if err != nil {
				fmt.Printf("Error running script %v: %v\n", scriptFile, err)
				return
			}
		}
	*/
	var UISyncLock sync.Mutex
	//	refreshTimer := time.NewTicker(time.Second * 20)
	refreshTicker := time.NewTicker(time.Second * 30)
	//retileCheckTicker := time.NewTicker(time.Second * 20)
	retileRequests := make(map[string]struct{})
	_ = retileRequests

	nextRefreshForce := false
	changed := false
	uiEvts := make(chan Evt, 100)
	var uiUpdatesRunning int32
	var initPending = true
EvtLoop:
	for {
		inst.WMBE.State(nextRefreshForce)
		if changed && false {
			wms := inst.WMBE.State(false)
			fmt.Printf("-----------------------\n")
			for _, s := range wms.Screens {
				fmt.Printf("%v - %v - %v\n", s.ID, s.ActiveTag, s.Tags)
				for _, w := range wms.Windows {
					if !w.IsDesktop && !w.Special && w.PrimaryScreen == s.ID {
						fmt.Printf("%v\n", w.WindowInfo())
					}
				}
			}
		}
		nextRefreshForce = false
		if !initPending {
			if atomic.LoadInt32(&uiUpdatesRunning) == 0 {
				atomic.StoreInt32(&uiUpdatesRunning, 1)
				go func() {
					time.Sleep(time.Millisecond * 20)
					UISyncLock.Lock() // this lock ensures that UIs aren't updated concurrently, the go func ensures this doesn`t block the main functionality
					doUpdates := atomic.LoadInt32(&uiUpdatesRunning) != 0
					pendingEvts := []Evt{}
				readEvts:
					for {
						select {
						case e := <-uiEvts:
							pendingEvts = append(pendingEvts, e)
						default:
							break readEvts
						}
					}
					//				fmt.Printf("UIUpd %v\n", len(pendingEvts))
					if doUpdates {
						wms := inst.WMBE.State(true)
						for _, ui := range inst.UIBackends {
							ui.ScreenUpdate(wms, pendingEvts)
						}
					}
					UISyncLock.Unlock()
					atomic.StoreInt32(&uiUpdatesRunning, 0)
				}()
			}
		}
		select {
		case <-refreshTicker.C:
			nextRefreshForce = true
			//		case <-retileCheckTicker.C:
			if len(retileRequests) > 0 {
				tnow := time.Now()
				if tnow.After(inst.RetileLockout) {
					for s := range retileRequests {
						delete(retileRequests, s)
						wms := inst.WMBE.State(false)
						sp := inst.GetScreenParams(s, false)
						scr := wms.GetScreen(s)
						if scr != nil {
							if sp.Tiler != nil {
								inst.CallTiler(scr, nil, sp.Tiler, "")
								changed = true
							}
						}
					}
				}
			}
		case evt := <-inst.WMBE.EvtChan():
			fmt.Printf("Evt %v=n\n", evt)
			if evt.Type == EvtTypeQuit {
				break EvtLoop
			}
			if evt.Type != "" {
				if evt.Type == EvtTypeInit || !initPending {
					//					inst.Log("EVT %v:%v", evt.Type, evt.Data)
					inst.EvtType = evt.Type
					inst.EvtData = evt.Data
					if evt.Type == EvtTypeWindowGeometry {
						inst.RetileLockout = time.Now().Add(time.Millisecond * 50)
						wms := inst.WMBE.State(false)
						for _, scr := range wms.Screens {
							sp := inst.GetScreenParams(scr.ID, false)
							if sp.TilerAutomatic && sp.Tiler != nil {
								retileRequests[scr.ID] = struct{}{}
							}
						}
					}
					if lp, ok := evt.Params.(lua.LValue); ok {
						inst.EvtParam = lp
						if lp.Type() == lua.LTFunction {
							inst.L.Push(lp)
							err = inst.L.PCall(0, lua.MultRet, nil)
							if err != nil {
								inst.Log(logfunk.Error, "Error running script %v: %v\n", scriptFile, err)
							}
						}
					}
					if evt.Type == EvtTypeWindowGeometry {
						changed = true
						wms := inst.WMBE.State(false)
						newWndID := evt.Data
						wnd := wms.GetWindow(newWndID)
						if wnd != nil {
							scr := wms.GetScreen(wnd.PrimaryScreen)
							if scr != nil {
								inst.WMBE.SetWindowTag(newWndID, scr.ActiveTag)
							}
						}
					}
					if evt.Type == EvtTypeWindowActivate {
						wms := inst.WMBE.State(false)
						newWndID := evt.Data
						wnd := wms.GetWindow(newWndID)
						if wnd != nil {
							scr := wms.GetScreen(wnd.PrimaryScreen)
							if scr != nil {
								if scr.ActiveTag != "" && wnd.Tag != "" && wnd.Tag != scr.ActiveTag {
									scrHasTag := false
									for _, t := range scr.Tags {
										if t == wnd.Tag {
											scrHasTag = true
										}
									}
									if scrHasTag {
										inst.WMBE.SetScreenTag(scr.ID, wnd.Tag)
										inst.ApplyTagVisiblity(false, "")
									}
								}
							}
						}
					}
					if evt.Type == EvtTypeWindowOpen {
						changed = true
						wms := inst.WMBE.State(false)
						newWndID := evt.Data
						wnd := wms.GetWindow(newWndID)
						if wnd != nil {
							scr := wms.GetScreen(wnd.PrimaryScreen)
							if scr != nil {
								inst.WMBE.SetWindowTag(newWndID, scr.ActiveTag)
							}
						}
					}
					if evt.Type == EvtTypeWindowOpen || evt.Type == EvtTypeWindowClose {
						wms := inst.WMBE.State(false)
						for _, scr := range wms.Screens {
							sp := inst.GetScreenParams(scr.ID, false)
							if sp.TilerAutomatic && sp.Tiler != nil {
								retileRequests[scr.ID] = struct{}{}
							}
						}
					}

					inst.L.Push(inst.Script)
					err = inst.L.PCall(0, lua.MultRet, nil)
					if err != nil {
						inst.Log(logfunk.Error, "Error running script %v: %v\n", scriptFile, err)
					}
					if len(uiEvts) < cap(uiEvts) {
						uiEvts <- evt
					}
					initPending = false
				}
			}
			inst.EvtType = ""
			inst.EvtData = ""
			inst.EvtParam = nil
		}
	}

	inst.L.Close()
	inst.WMBE.Shut()

	fmt.Printf("quit\n")
}
