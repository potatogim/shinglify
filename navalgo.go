package shinglify

import (
	"fmt"
	"sort"

	lua "github.com/yuin/gopher-lua"
)

func (inst *Inst) NavNextWindow(wsl *WMState, from WindowID, dir Direction) (nxtid WindowID) {
	fromidx := wsl.FindWindowIndex(from)
	defer func() {
		//		fmt.Printf("navnext %v  >  %v\n", wsl.WindowInfo(from), wsl.WindowInfo(nxtid))
	}()
	if fromidx == -1 {
		return ""
	}
	wf := wsl.Windows[fromidx]
	wcl := FilterReachable(wsl, from, dir >= DirNext)

	if dir >= DirNext {
		sort.Slice(wcl, func(i, j int) bool { return (wcl[i].PrimaryScreen + wcl[i].ID) < (wcl[j].PrimaryScreen + wcl[j].ID) })
	}

	if false {
		for _, wc := range wcl {
			fmt.Printf("Reachable: %5v  -  %v  -  %v\n", wc.ID == from, wc.PrimaryScreen+wc.ID, wsl.WindowInfo(wc.ID))
		}
	}

	if dir == DirNextTagOnScreen {
		scr := wsl.GetScreen(wf.PrimaryScreen)
		if scr == nil {
			return ""
		}
		curTag := scr.ActiveTag
		nxtTag := curTag
		for i := 0; i < len(scr.Tags); i++ {
			if scr.Tags[i] == curTag {
				nxtTag = scr.Tags[(i+1)%len(scr.Tags)]
			}
		}
		if nxtTag == curTag {
			return from
		}
		inst.WMBE.SetScreenTag(wf.PrimaryScreen, nxtTag)
		for _, w := range wsl.Windows {
			if !w.Special && !w.IsDesktop && !w.Panel && w.Tag == nxtTag {
				return w.ID
			}
		}
		return ""
	}
	if dir == DirPrevTagOnScreen {
		scr := wsl.GetScreen(wf.PrimaryScreen)
		if scr == nil {
			return ""
		}
		curTag := scr.ActiveTag
		nxtTag := curTag
		for i := 0; i < len(scr.Tags); i++ {
			if scr.Tags[i] == curTag {
				nxtTag = scr.Tags[(i+len(scr.Tags)-1)%len(scr.Tags)]
			}
		}
		if nxtTag == curTag {
			return from
		}
		inst.WMBE.SetScreenTag(wf.PrimaryScreen, nxtTag)
		for _, w := range wsl.Windows {
			if !w.Special && !w.IsDesktop && !w.Panel && w.Tag == nxtTag {
				return w.ID
			}
		}
	}

	wclidx := -1
	for idx := range wcl {
		if wcl[idx].ID == from {
			wclidx = idx
			break
		}
	}

	if dir == DirNext {
		idx := wclidx + 1
		for {
			if idx >= len(wcl) {
				idx = 0
			}
			if wcl[idx].ID == from {
				return from
			}
			return wcl[idx].ID
		}
	}
	if dir == DirNextOnScreen {
		idx := wclidx + 1
		for {
			if idx >= len(wcl) {
				idx = 0
			}
			if wcl[idx].ID == from {
				return from
			}
			if wcl[idx].PrimaryScreen != wf.PrimaryScreen {
				idx++
				continue
			}
			return wcl[idx].ID
		}
	}
	if dir == DirNextOnTag {
		idx := wclidx + 1
		for {
			if idx >= len(wcl) {
				idx = 0
			}
			if wcl[idx].ID == from {
				return from
			}
			if wcl[idx].Tag != wf.Tag {
				idx++
				continue
			}
			return wcl[idx].ID
		}
	}
	if dir == DirPrev {
		idx := wclidx - 1
		for {
			if idx < 0 {
				idx = len(wcl) - 1
			}
			if wcl[idx].ID == from {
				return from
			}
			return wcl[idx].ID
		}
	}
	if dir == DirPrevOnScreen {
		idx := wclidx - 1
		for {
			if idx < 0 {
				idx = len(wcl) - 1
			}
			if wcl[idx].ID == from {
				return from
			}
			if wcl[idx].PrimaryScreen != wf.PrimaryScreen {
				idx--
				continue
			}
			return wcl[idx].ID
		}
	}
	if dir == DirPrevOnTag {
		idx := wclidx - 1
		for {
			if idx < 0 {
				idx = len(wcl) - 1
			}
			if wcl[idx].ID == from {
				return from
			}
			if wcl[idx].Tag != wf.Tag {
				idx--
				continue
			}
			return wcl[idx].ID
		}
	}

	/*
		for i, wc := range wcl {
			fmt.Printf("Cand %02v %v\n", i, wc.WindowInfo())
		}
	*/
	type score struct {
		title string
		id    WindowID
		match int
	}

	scoreboard := make(map[WindowID]*score)

	for _, wc := range wcl {
		scoreboard[wc.ID] = &score{id: wc.ID, match: 0, title: wc.Title}
	}

	scanx := 0
	scany := 0
	iterl := 0
	iterx := 0
	itery := 0
	startx := 0
	starty := 0

	margin := 150

	switch dir {
	case DirLeft:
		startx = wf.Geometry.X - 1
		starty = wf.Geometry.Y - margin
		iterl = wf.Geometry.H + margin*2
		scanx = -1
		scany = 0
		iterx = 0
		itery = 1
	case DirRight:
		startx = wf.Geometry.X + wf.Geometry.W
		starty = wf.Geometry.Y - margin
		iterl = wf.Geometry.H + margin*2
		scanx = 1
		scany = 0
		iterx = 0
		itery = 1
	case DirUp:
		startx = wf.Geometry.X - margin
		starty = wf.Geometry.Y - 1
		iterl = wf.Geometry.W + margin*2
		scanx = 0
		scany = -1
		iterx = 1
		itery = 0
	case DirDown:
		startx = wf.Geometry.X - margin
		starty = wf.Geometry.Y + wf.Geometry.H
		iterl = wf.Geometry.W + margin*2
		scanx = 0
		scany = 1
		iterx = 1
		itery = 0
	}

	for iter := 0; iter < iterl; iter += 5 {
		for scan := 1; scan < 35000; scan += 5 {
			x := startx + iter*iterx + scanx*scan
			y := starty + iter*itery + scany*scan
			for _, wc := range wcl {
				if wc.VisibleGeometry.Hit(Pnt{X: x, Y: y}) {
					dscr := 350000000 - scan*scan
					if scan > 200 && dscr > 10 {
						dscr = dscr / 10
					}
					if dscr > 0 {
						scoreboard[wc.ID].match += dscr
						scan = 35000
					}
				}
			}
		}
	}

	var prevActive1 WindowID
	var prevActive2 WindowID
	var prevActive3 WindowID
	awhl := len(wsl.ActiveWindowHistory)
	if awhl >= 1 {
		prevActive1 = wsl.ActiveWindowHistory[awhl-1]
	}
	if awhl >= 2 {
		prevActive2 = wsl.ActiveWindowHistory[awhl-2]
	}
	if awhl >= 3 {
		prevActive3 = wsl.ActiveWindowHistory[awhl-3]
	}
	var bestID WindowID
	var bestMatch = 1
	for _, sb := range scoreboard {
		score := sb.match
		if sb.id == prevActive1 {
			score = (score * 13) / 10
		}
		if sb.id == prevActive2 {
			score = (score * 12) / 10
		}
		if sb.id == prevActive3 {
			score = (score * 11) / 10
		}
		if score > bestMatch {
			bestID = sb.id
			bestMatch = score
		}
	}

	return bestID
}

func NavRangesOverlap(a1, a2, b1, b2 int) bool {
	return (b1 >= a1 && b1 <= a2) || (b2 >= a1 && b2 <= a2) || ((b1 < a1) && (b2 > a2))
}

func (inst *Inst) LuaNavigateLeft(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirLeft)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigateRight(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirRight)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigateUp(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirUp)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigateDown(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirDown)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigateNext(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirNext)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigatePrev(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirPrev)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigateNextOnScreen(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirNextOnScreen)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigatePrevOnScreen(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirPrevOnScreen)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigateNextOnTag(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirNextOnTag)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigatePrevOnTag(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirPrevOnTag)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigateNextTagOnScreen(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirNextTagOnScreen)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaNavigatePrevTagOnScreen(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	nxtid := inst.NavNextWindow(&wms, wndid, DirPrevTagOnScreen)
	if nxtid != "" {
		inst.WMBE.Activate(nxtid)
	}
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaWindowMoveLeft(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	inst.NavMoveWindow(wndid, DirLeft)
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaWindowMoveRight(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	inst.NavMoveWindow(wndid, DirRight)
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaWindowMoveUp(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	inst.NavMoveWindow(wndid, DirUp)
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) LuaWindowMoveDown(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := wms.GetActiveWindowID()
	inst.WMBE.BeginChange()
	inst.NavMoveWindow(wndid, DirDown)
	inst.WMBE.EndChange()
	return 0
}

func (inst *Inst) NavMoveWindow(wndid WindowID, dir Direction) bool {
	inst.Log("NavMoveWindow %v %v", wndid, dir)
	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wndid)
	if wnd == nil {
		return false
	}
	wasMaximized := wnd.Maximized
	curscrid := wnd.PrimaryScreen
	scr := wms.GetScreen(curscrid)
	if scr == nil {
		return false
	}
	sg := scr.Geometry
	var xb, yb, rng, xd, yd, xi, yi int
	switch dir {
	case DirLeft:
		xb = sg.X
		yb = sg.Y + sg.H/2
		rng = sg.H/2 - 1
		xd = 0
		yd = 1
		xi = -1
		yi = 0
	case DirRight:
		xb = sg.X + sg.W - 1
		yb = sg.Y + sg.H/2
		rng = sg.H/2 - 1
		xd = 0
		yd = 1
		xi = 1
		yi = 0
	case DirUp:
		xb = sg.X + sg.W/2
		yb = sg.Y
		rng = sg.W/2 - 1
		xd = 1
		yd = 0
		xi = 0
		yi = -1
	case DirDown:
		xb = sg.X + sg.W/2
		yb = sg.Y + sg.H - 1
		rng = sg.W/2 - 1
		xd = 1
		yd = 0
		xi = 0
		yi = 1
	default:
		return false
	}
	nxtscrid := ""
	samples := 100
sampleloop:
	for samples > 0 {
		samples--
		for r := -rng; r <= rng; r += 10 {
			x := xb + xd*r + xi*samples
			y := yb + yd*r + yi*samples
			for _, tscr := range wms.Screens {
				if tscr.ID != scr.ID && tscr.Geometry.Hit(Pnt{X: x, Y: y}) {
					nxtscrid = tscr.ID
					break sampleloop
				}
			}
		}
	}
	if nxtscrid == "" {
		return false
	}
	inst.MoveWindowToScreen(wndid, nxtscrid)
	if wasMaximized {
		inst.WMBE.Maximize(wndid, 1)
	} else {
	}
	return true
}

func (inst *Inst) MoveWindowToScreen(wndid WindowID, scrid ScreenID) {
	inst.Log("Move %v to %v", wndid, scrid)
	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wndid)
	scr := wms.GetScreen(scrid)
	if wnd == nil || scr == nil {
		return
	}
	wndw := wnd.Geometry.W
	wndh := wnd.Geometry.H
	if wndw > scr.Geometry.W {
		wndw = scr.Geometry.W
	}
	if wndh > scr.Geometry.H {
		wndh = scr.Geometry.H
	}
	var rct Rct
	rct.X = scr.Geometry.X + (scr.Geometry.W-wndw)/2
	rct.Y = scr.Geometry.Y + (scr.Geometry.H-wndh)/2
	rct.W = wndw
	rct.H = wndh
	inst.WMBE.Move(wndid, rct)
	inst.WMBE.State(true)
	sp := inst.GetScreenParams(scrid, false)
	if sp.TilerAutomatic && sp.Tiler != nil {
		inst.CallTiler(scr, nil, sp.Tiler, "")
	}

}
