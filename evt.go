package shinglify

import "time"

type Evt struct {
	Timestamp time.Time
	Type      string
	Data      string
	Params    interface{}
}

const EvtTypeScreenChange = "ScreenChange"
const EvtTypeWindowChange = "WindowChange"
const EvtTypeWindowActivate = "WindowActivate"
const EvtTypeWindowOpen = "WindowOpen"
const EvtTypeWindowClose = "WindowClose"
const EvtTypeWindowGeometry = "WindowGeometry"
const EvtTypeWindowTitle = "WindowTitle"
const EvtTypeKeyPress = "KeyPress"
const EvtTypeKeyRelease = "KeyRelease"
const EvtTypeError = "Error"
const EvtTypeRaw = "Raw"
const EvtTypeInit = "Init"
const EvtTypeQuit = "Quit"
