package shinglify

import (
	"fmt"
	"strings"
)

type WMWindowState struct {
	ID       WindowID
	Title    string
	Types    []string
	States   []string
	Class    string
	Instance string
	Desktop  string
	Geometry Rct
	//	InnerGeometry   Rct
	PosOfs          Pnt
	SizeOfs         Pnt
	VisibleGeometry RctList
	Visible         bool
	Active          bool
	Maximized       bool
	Special         bool
	Panel           bool
	IsDesktop       bool
	IconID          string

	PrimaryScreen string
	SingleScreen  bool
	Tag           Tag
}

type WMWindowStateList []WMWindowState

type WMWindowIDList []WindowID

type WMScreenState struct {
	ID           ScreenID
	Name         string
	Geometry     Rct
	Manufacturer string
	Product      string
	Serial       string
	Tags         TagList
	ActiveTag    Tag
}

type WMScreenStateList []WMScreenState

type WMScreenIDList []ScreenID

type WMDesktopState struct {
	ID       string
	Name     string
	Geometry Rct
	Active   bool
}

type WMDesktopStateList []WMDesktopState

type WMState struct {
	WMName              string
	Windows             WMWindowStateList
	Screens             WMScreenStateList // all outputs that are currently enabled and mapped
	Desktops            WMDesktopStateList
	Outputs             WMScreenStateList // all outputs, including disabled ones
	ScreensConnected    string
	ScreenLayouts       string
	ActiveWindowHistory []WindowID
}

func (w *WMWindowState) WindowInfo() string {
	extras := []string{}
	if w.Active {
		extras = append(extras, "A")
	}
	if w.Visible {
		extras = append(extras, "V")
	}
	if w.Panel {
		extras = append(extras, "P")
	}
	if w.Special {
		extras = append(extras, "S")
	}
	if w.Tag != "" {
		extras = append(extras, "T:"+w.Tag)
	}
	if w.PrimaryScreen != "" {
		extras = append(extras, "D:"+w.PrimaryScreen)
	}
	return fmt.Sprintf("%11v [%v] X:%v+%v=%v Y:%v+%v=%v C:%v T:%v", w.ID, strings.Join(extras, ","),
		w.Geometry.X, w.Geometry.W, w.Geometry.X+w.Geometry.W, w.Geometry.Y, w.Geometry.H, w.Geometry.Y+w.Geometry.H, w.Class, w.Title)
}

func (wsl *WMState) WindowInfo(wndid WindowID) string {
	for _, w := range wsl.Windows {
		if w.ID == wndid {
			return w.WindowInfo()
		}
	}
	return "INVALID"
}

func (wsl *WMState) GetWindow(wndid WindowID) *WMWindowState {
	for _, w := range wsl.Windows {
		if w.ID == wndid {
			return &w
		}
	}
	return nil
}

func (wsl *WMState) GetScreen(scrid ScreenID) *WMScreenState {
	for _, s := range wsl.Screens {
		if s.ID == scrid {
			return &s
		}
	}
	return nil
}

func (wsl *WMState) FindWindowIndex(wndid WindowID) int {
	for i, w := range wsl.Windows {
		if w.ID == wndid {
			return i
		}
	}
	return -1
}

func (wsl *WMState) GetActiveWindowID() WindowID {
	for _, w := range wsl.Windows {
		if w.Active {
			return w.ID
		}
	}
	return ""
}

func (wsl *WMState) GetScreenForTag(tag Tag) ScreenID {
	for _, s := range wsl.Screens {
		for _, t := range s.Tags {
			if t == tag {
				return s.ID
			}
		}
	}
	return ""
}
