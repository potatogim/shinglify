package shinglify

func FilterReachable(wsl *WMState, from WindowID, includeOccluded bool) WMWindowStateList {
	r := WMWindowStateList{}
	wfrom := wsl.GetWindow(from)
	if wfrom == nil {
		return r
	}
	for _, wc := range wsl.Windows {
		if wc.Visible && !wc.Panel && !wc.Special {
			if len(wc.VisibleGeometry.Rcts) > 0 || includeOccluded {
				if wc.VisibleGeometry.Area()*4 >= wc.Geometry.W*wc.Geometry.H*3 {
					if wfrom.Desktop != "" {
						if wc.Desktop != "" && wc.Desktop != wfrom.Desktop {
							continue
						}
					}
					r = append(r, wc)
				}
			}
		}
	}
	return r
}
