package shinglify

type Direction int

const (
	DirNone  Direction = 0
	DirLeft  Direction = 1
	DirRight Direction = 2
	DirUp    Direction = 3
	DirDown  Direction = 4

	DirNext            Direction = 10
	DirPrev            Direction = 11
	DirNextOnScreen    Direction = 20
	DirPrevOnScreen    Direction = 21
	DirNextOnTag       Direction = 30
	DirPrevOnTag       Direction = 31
	DirNextTagOnScreen Direction = 40
	DirPrevTagOnScreen Direction = 41
)
